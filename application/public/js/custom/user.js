var dataUsers = {},
    tblUsers = null;

$(function() {
  if ($("#tbl-users").length > 0) {

    // --------------------------------------------------------------------------
    // --------------------------------------------------------------------------
    // ---------------------------- TABLA USUARIOS ------------------------------
    // --------------------------------------------------------------------------
    tblUsers = $('#tbl-users').DataTable({
      'paging': true,
      'info': true,
      'filter': true,
      'stateSave': true,
      'processing': true, 
      'serverSide': true,
      'lengthMenu' : [[10, 30, 50, -1], [10, 30, 50, 'Todo']], // edita el menu de cantidad de registros que se muestran
      dom: 'Bfrtlip',
      buttons: [
          {
              extend: 'excelHtml5',
              text: str_exportExcel,
              title: str_exportTitle,
              exportOptions: {
                  columns: [0, 1, 2, 3]
              }
          },
          {
              extend: 'pdfHtml5',
              text: str_exportPdf,
              title: str_exportTitle,
              exportOptions: {
                  columns: [0, 1, 2, 3]
              }
          }
      ],
      'ajax': {
        'url': '/user/ssp-list-of-users',
        'type': 'POST',
        'data': function (d) {
          return $.extend({}, d, {
            'dateStart': $('#dateStart').val(),
            'dateEnd': $('#dateEnd').val(),
          } );
        },
      },
      'columns': [
        {data: 'id', 'sClass': 'dt-body-center'},
        {data: 'username', 'sClass': 'dt-body-center'},
        {data: 'email', 'sClass': 'dt-body-center'},
        {data: 'phone', 'sClass': 'dt-body-center'},
        {'orderable': true,
          render:function(data, type, row) {

            dataUsers[row.id] = row;
            
            //var daraUser = JSON.stringify(row);
            //var daraUser = JSON.parse(row);

            return '<a href="#" data-toggle="modal" data-target="#modal-edit-user" onclick="editUser(\'' + row.id+ '\');">' +
                      '<i class="fa fa-cog fa-2x" aria-hidden="true"></i>' +
                    '</a>';       
          },
          'sClass': 'dt-body-center'
        }
      ],
       "oLanguage": {
          "sProcessing":     oLanguage.sProcessing,
          "sLengthMenu":     oLanguage.sLengthMenu,
          "sZeroRecords":    oLanguage.sZeroRecords,
          "sEmptyTable":     oLanguage.sEmptyTable,
          "sInfo":           oLanguage.sInfo,
          "sInfoEmpty":      oLanguage.sInfoEmpty,
          "sInfoFiltered":   oLanguage.sInfoFiltered,
          "sInfoPostFix":    "",
          "sSearch":         oLanguage.sSearch,
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": oLanguage.sLoadingRecords,
          "oPaginate": {
              "sFirst":    oLanguage.oPaginate.sFirst,
              "sLast":     oLanguage.oPaginate.sLast,
              "sNext":     oLanguage.oPaginate.sNext,
              "sPrevious": oLanguage.oPaginate.sPrevious
          },
          "oAria": {
              "sSortAscending": oLanguage.oAria.sSortAscending,
              "sSortDescending": oLanguage.oAria.sSortDescending
          }
      },
      "language": {
        "url": "dataTables.german.lang"
      },
      'order': [[0, 'desc']],
      'columnDefs': [
        {
          'targets': [1],
          'data': 'username',
          'render': function(data, type, row) {
            var DifferenceDays = getDifferenceDays(row.created_on_short, getToday());
            var newUser = (DifferenceDays == 0 || DifferenceDays == 1) ? '<small class="label label-success pull-left">New</small>' : '';
            return newUser + ' ' + row.username + '<small class="text-primary"> [' + row.name_role + ']</smal>';
          },
        },
      ],
      /*"initComplete": function(settings, json) {
        //console.log(json.tokenAjax.csrf_hash);
        // Regenerar token csrf para nueva peticion ajax
        // nombre: json.tokenAjax.csrf_name
        $('#token-ajax').val(json.tokenAjax.csrf_hash);
      },*/
      
    });

    $("#tbl-users_filter").append('<label>' +
          '<span style="margin-left:5px;">' + str_start + '</span>' +
          '<input type="text" id="dateStart" name="dateStart" readonly="true">' +
        '</label>' +
        '<label>' +
          '<span style="margin-left:5px;">' + str_end + '</span>' +
          '<input type="text" id="dateEnd" name="dateEnd" readonly="true">' +
        '</label>');

  
    $("#dateStart, #dateEnd").datepicker({
      changeMonth: true,
      changeYear: true,
      regional: 'es',
      dateFormat: 'yy-mm-dd',
      showButtonPanel: true,
      closeText: str_closeText,
      onClose: function () {
        var event = arguments.callee.caller.caller.arguments[0];

        if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
          $("#dateStart").val('');
          $("#dateEnd").val('');
          tblUsers.draw();
        }
      }
    });

    $('#dateStart, #dateEnd').change(function () {
      if ($("#dateStart").val() != "" && $("#dateEnd").val() != "") {
        tblUsers.draw();
      }
    });
  }// endif table-users
});


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ------------------- SETEAR DATOS EDITAR USUARIO --------------------------
// --------------------------------------------------------------------------
function editUser(id) {  
  clearInputs();
  //console.log(dataUsers[id]);
  $('#id-modal').val(id);
  $('#username-modal').val(dataUsers[id].username);
  $('#email-modal').val(dataUsers[id].email);
  $('#phone-modal').val(dataUsers[id].phone);
  $('#first_name-modal').val(dataUsers[id].first_name);
  $('#last_name-modal').val(dataUsers[id].last_name);
  $('#last_login-modal').val(dataUsers[id].last_login);

  if (dataUsers[id].active) {
    $('#active-modal').prop('checked', true);
  }

  $('#roles_id-modal option[value="' + dataUsers[id].roles_id + '"]').attr("selected",true);
}


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ---------------------- ACTUALIZAR INFO DE USUARIO ------------------------
// --------------------------------------------------------------------------
$('.page-list-of-users').on('click', '#save-edit-user', function() {
  $('#msg-update-user').html('');
  
  var form = document.getElementById("form-edit-user");
  if (form.reportValidity()) {
    $.ajax({
      type: 'POST',
      url: $('#form-edit-user').attr('action'),
      data: $('#form-edit-user').serialize(),
      beforeSend: function(){
        $(this).siblings('.loading').show();      
      },
      success: function(data){
        try {
          var dataJson = JSON.parse(data);
          $('#form-edit-user input[name="csrf_test_name"]').val(dataJson.update_token_ajax.csrf_hash);
          $('#msg-update-user').html(dataJson.msg).fadeIn('slow');
          tblUsers.draw();
        } catch (e) {
          $('#msg-update-user').html(renderMsgError('Error', 'Forbidden')).fadeIn('slow');
        }
      },   
      complete: function(){
        $(this).siblings('.loading').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $('#msg-update-user').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
        //alert("Status: " + textStatus + "Error: " + errorThrown); 
      }
    });
  }
});


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// -------------------- ACTUALIZAR SEGURIDAD DE USUARIO ---------------------
// --------------------------------------------------------------------------
$('.page-list-of-users').on('click', '#update-security-user', function() {
  $('#msg-update-security-user').html('');
  var form = document.getElementById("form-security-user");
  if (form.reportValidity()) {
    $.ajax({
      type: 'POST',
      url: $('#form-security-user').attr('action'),
      data: {
        id: $('#id-modal').val(),
        new_password: $('#new_password-modal').val(),
        repeat_new_password: $('#repeat_new_password-modal').val(),
        csrf_test_name: $('#form-edit-user input[name="csrf_test_name"]').val(),
      },
      beforeSend: function(){
        $(this).siblings('.loading').show();      
      },
      success: function(data){
        try {
          var dataJson = JSON.parse(data);
          $('#form-edit-user input[name="csrf_test_name"]').val(dataJson.update_token_ajax.csrf_hash);
          $('#msg-update-security-user').html(dataJson.msg).fadeIn('slow');
          tblUsers.draw();
        } catch (e) {
          $('#msg-update-security-user').html(renderMsgError('Error', 'Forbidden')).fadeIn('slow');
        }
      },   
      complete: function(){
        $(this).siblings('.loading').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $('#msg-update-security-user').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
      }
    });
  }
});


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// -------------------------- ELIMINAR USUARIO ------------------------------
// --------------------------------------------------------------------------
$('.page-list-of-users').on('click', '#delete-user', function() {
  //$('#msg-update-security-user').html('');

  var check = confirm(str_check);
      
  if(check === true) {
    $.ajax({
      type: 'POST',
      url: $('#form-delete-user').attr('action'),
      data: {
        id: $('#id-modal').val(),
        csrf_test_name: $('#form-edit-user input[name="csrf_test_name"]').val(),
      },
      beforeSend: function(){
        $(this).siblings('.loading').show();      
      },
      success: function(data){
        try {
          tblUsers.draw();
          var dataJson = JSON.parse(data);
          $('#form-edit-user input[name="csrf_test_name"]').val(dataJson.update_token_ajax.csrf_hash);
          $('#modal-edit-user').modal('hide');
          $('.msg-ajax').html(dataJson.msg).fadeIn('slow');
        } catch (e) {
          $('#msg-delete-security-user').html(renderMsgError('Error', 'Forbidden')).fadeIn('slow');
        }
      },   
      complete: function(){
        $(this).siblings('.loading').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $('#msg-delete-security-user').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
      }
    });
  }
});


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// -------------------------- CREAR USUARIO ------------------------------
// --------------------------------------------------------------------------
$('.page-list-of-users').on('click', '#create-user', function() {

  var form = document.getElementById("form-create-user");
  if (form.reportValidity()) {
    $.ajax({
      type: 'POST',
      url: $('#form-create-user').attr('action'),
      data: $('#form-create-user').serialize(),
      beforeSend: function(){
        $(this).siblings('.loading').show();      
      },
      success: function(data){
        console.log(data);
        try {
          var dataJson = JSON.parse(data);
          if (dataJson.ok) {
            window.location.href = "/user/list-of-users";
          }else {
            $('#form-create-user input[name="csrf_test_name"]').val(dataJson.update_token_ajax.csrf_hash);
            //$('#modal-create-user').modal('hide');
            $('#msg-create-user').html(dataJson.msg).fadeIn('slow');
          }

          
        }catch (e) {
          $('#msg-create-user').html(renderMsgError('Error', 'Forbidden')).fadeIn('slow');
        }
      },   
      complete: function(){
        $(this).siblings('.loading').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $('#msg-create-user').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
      }
    });
  }
});


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ----------------- RENDERIZAR MSG DE ERRORES AJAX -------------------------
// --------------------------------------------------------------------------
function renderMsgError(textStatus, errorThrown) {
  return '<div>' +
            '<p class="text-danger">' + str_access_denied + '</p>'+
            '<button type="button" class="btn btn-link" data-toggle="collapse" data-target="#show-more-msg">' + str_show_more + '</button>' +
            '<div id="show-more-msg" class="collapse">' +
              '<p>Status: ' + textStatus + '</p>' +
              '<p>Error: ' + errorThrown + '</p>' +
            '</div>'
          '</div>';

}

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ---------------------- LIMPIAR IMPUTS DE MODALS --------------------------
// --------------------------------------------------------------------------
function clearInputs() {  
  // datos de usuario
  $('#id-modal').val('');
  $('#username-modal').val('');
  $('#email-modal').val('');
  $('#phone-modal').val('');
  $('#first_name-modal').val('');
  $('#last_name-modal').val('');
  $('#last_login-modal').val('');
  $('#active-modal').prop('checked', false);
  //$('#roles_id-modal option[value="' + dataUsers[id].roles_id + '"]').attr("selected",true);

  // seguridad de usuario
  $('#new_password-modal').val('');
  $('#repeat_new_password-modal').val('');
}


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// --------- OBTENER DIFERENCIA DE DIAS ENTRE 2 FECHAS ----------------------
// --------------------------------------------------------------------------
getDifferenceDays = function(f1,f2) {
  var aFecha1 = f1.split('-'); 
  var aFecha2 = f2.split('-'); 
  var fFecha1 = Date.UTC(aFecha1[0],aFecha1[1]-1,aFecha1[2]); 
  var fFecha2 = Date.UTC(aFecha2[0],aFecha2[1]-1,aFecha2[2]); 
  var dif = fFecha2 - fFecha1;
  var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
   return dias;
}


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// -------------------------- OBTENER FECHA DE HOY --------------------------
// --------------------------------------------------------------------------
function getToday() {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();

  if(dd<10) {
    dd = '0'+dd
  } 

  if(mm<10) {
    mm = '0'+mm
  } 

  return yyyy + '-' + mm + '-' + dd;
}


