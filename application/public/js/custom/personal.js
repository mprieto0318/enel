var dataOBJ = {},
    tbl = null;

$(function() {
  if ($("#tbl-" + str_MODULE).length > 0) {

    // --------------------------------------------------------------------------
    // --------------------------------------------------------------------------
    // ---------------------------- TABLA ------------------------------
    // --------------------------------------------------------------------------
    tbl = $('#tbl-' + str_MODULE).DataTable({
      'paging': true,
      'info': true,
      'filter': true,
      'stateSave': true,
      'processing': true, 
      'serverSide': true,
      'lengthMenu' : [[10, 30, 50, -1], [10, 30, 50, 'Todo']], // edita el menu de cantidad de registros que se muestran
      dom: 'Bfrtlip',
      buttons: [
          {
              extend: 'excelHtml5',
              text: str_exportExcel,
              title: str_exportTitle,
              exportOptions: {
                  columns: [0, 1, 2, 3]
              }
          },
          {
              extend: 'pdfHtml5',
              text: str_exportPdf,
              title: str_exportTitle,
              exportOptions: {
                  columns: [0, 1, 2, 3]
              }
          }
      ],
      'ajax': {
        'url': '/' + str_URL_1 + '/ssp-list',
        'type': 'POST',
        'data': function (d) {
          return $.extend({}, d, {
            'dateStart': $('#dateStart').val(),
            'dateEnd': $('#dateEnd').val(),
          } );
        },
      },
      'columns': [
        {data: 'name', 'sClass': 'dt-body-center'},
        {data: 'cc', 'sClass': 'dt-body-center'},
        {data: 'name_empresa', 'sClass': 'dt-body-center'},
        {data: 'name_cargo', 'sClass': 'dt-body-center'},
        {data: 'name_nivel_academico', 'sClass': 'dt-body-center'},
        {data: 'created_on_short', 'sClass': 'dt-body-center'},
        {'orderable': true,
          render:function(data, type, row) {
            dataOBJ[row.id] = row;

            return '<a href="#" data-toggle="modal" data-target="#modal-edit" onclick="editRow(\'' + row.id+ '\');">' +
                      '<i class="fa fa-cog fa-2x" aria-hidden="true"></i>' +
                    '</a>';       
          },
          'sClass': 'dt-body-center'
        }
      ],
       "oLanguage": {
          "sProcessing":     oLanguage.sProcessing,
          "sLengthMenu":     oLanguage.sLengthMenu,
          "sZeroRecords":    oLanguage.sZeroRecords,
          "sEmptyTable":     oLanguage.sEmptyTable,
          "sInfo":           oLanguage.sInfo,
          "sInfoEmpty":      oLanguage.sInfoEmpty,
          "sInfoFiltered":   oLanguage.sInfoFiltered,
          "sInfoPostFix":    "",
          "sSearch":         oLanguage.sSearch,
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": oLanguage.sLoadingRecords,
          "oPaginate": {
              "sFirst":    oLanguage.oPaginate.sFirst,
              "sLast":     oLanguage.oPaginate.sLast,
              "sNext":     oLanguage.oPaginate.sNext,
              "sPrevious": oLanguage.oPaginate.sPrevious
          },
          "oAria": {
              "sSortAscending": oLanguage.oAria.sSortAscending,
              "sSortDescending": oLanguage.oAria.sSortDescending
          }
      },
      "language": {
        "url": "dataTables.german.lang"
      },
      'order': [[0, 'desc']],
      'columnDefs': [
        {
          'targets': [0],
          'data': 'name',
          'render': function(data, type, row) {
            var DifferenceDays = getDifferenceDays(row.created_on_short, getToday());
            var newRow = (DifferenceDays == 0 || DifferenceDays == 1) ? '<small class="label label-success pull-left">New</small>' : '';
            return newRow + ' ' + row.name + ' ' + row.surname + ' ' + row.second_surname;
          },
        },
      ],
      /*"initComplete": function(settings, json) {
        //console.log(json.tokenAjax.csrf_hash);
        // Regenerar token csrf para nueva peticion ajax
        // nombre: json.tokenAjax.csrf_name
        $('#token-ajax').val(json.tokenAjax.csrf_hash);
      },*/
      
    });

    $("#tbl-" + str_MODULE + "_filter").append('<label>' +
          '<span style="margin-left:5px;">' + str_start + '</span>' +
          '<input type="text" id="dateStart" name="dateStart" readonly="true">' +
        '</label>' +
        '<label>' +
          '<span style="margin-left:5px;">' + str_end + '</span>' +
          '<input type="text" id="dateEnd" name="dateEnd" readonly="true">' +
        '</label>');

  
    $("#dateStart, #dateEnd").datepicker({
      changeMonth: true,
      changeYear: true,
      regional: 'es',
      dateFormat: 'yy-mm-dd',
      showButtonPanel: true,
      closeText: str_closeText,
      onClose: function () {
        var event = arguments.callee.caller.caller.arguments[0];

        if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
          $("#dateStart").val('');
          $("#dateEnd").val('');
          tbl.draw();
        }
      }
    });

    $('#dateStart, #dateEnd').change(function () {
      if ($("#dateStart").val() != "" && $("#dateEnd").val() != "") {
        tbl.draw();
      }
    });
  }// endif table-users
});


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ------------------- SETEAR DATOS EDITAR --------------------------
// --------------------------------------------------------------------------
function editRow(id) {  
  clearInputs();

  $('#id-modal').val(id);
  $('#cc-modal').val(dataOBJ[id].cc);
  $('#surname-modal').val(dataOBJ[id].surname);
  $('#second_surname-modal').val(dataOBJ[id].second_surname);
  $('#name-modal').val(dataOBJ[id].name);

  $('#start_contract_date-modal').val(dataOBJ[id].start_contract_date_short);
  $('#professional_registration-modal').val(dataOBJ[id].professional_registration);

  if (dataOBJ[id].active == 1) {
    $('#active-modal').prop('checked', true);
  }else {
    $('#active-modal').prop('checked', false);
  }

  $('#empresa_id-modal option[value="' + dataOBJ[id].empresa_id + '"]').attr("selected",true);
  $('#cargos_id-modal option[value="' + dataOBJ[id].cargos_id + '"]').attr("selected",true);
  $('#nivel_academico_id-modal option[value="' + dataOBJ[id].nivel_academico_id + '"]').attr("selected",true);

  if (dataOBJ[id].img != null && dataOBJ[id].img != '') {
    $('#img-current').html('<br><b>Imagen Actual: </b><img width="45" height="45" src="./application/public/src/img-personal/' + dataOBJ[id].img + '">')

    $('#img_current_hidden-modal').val(dataOBJ[id].img);
  }else {
    $('#img-current').html('<small>No ha cargado ningun logo</small>')
    $('#img_current_hidden-modal').val('');
  }


  // Setear atributos para nuevas competencias
  $('#new-competencia').attr("data-personal-id", id);
  $('#new-competencia').attr("data-personal-name", dataOBJ[id].name + ' ' + dataOBJ[id].surname);

  // Get Competencias asociadas a la persona
  $.ajax({
      type: 'POST',
      url: '/personal/list-competencias',
      data: {
        id: id,
      },
      beforeSend: function(){
        //$(this).siblings('.loading').show();      
      },
      success: function(data){
        $('#list-competencias').html(data);
      },   
      complete: function(){
        //$(this).siblings('.loading').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        //$('#msg-update').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
        //alert("Status: " + textStatus + "Error: " + errorThrown); 
      }
    });
}


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ------------------- MODAL NUEVA COMPETENCIA --------------------------
// --------------------------------------------------------------------------
function newCompetencia() {
  $('#modal-create-competencia #name-person').text($('#new-competencia').attr("data-personal-name"));
  $('#modal-create-competencia #personal_id-modal').val($('#new-competencia').attr("data-personal-id"));

  // resetear valores
  $('#form-create-competencia #competencias_id-modal').show();
  $('#form-create-competencia #competencias_id-modal option').show();
  $('#msg-no_more_competitions').remove();
  $('#form-create-competencia #btn-new-competencias').attr('disabled', false);


  var listCompetenciasPersona = $('#list-competencias .edit-competencia').map(function(){return $(this).attr('data-id_competencia');}).get();

  var iOptions = 0,
      iOptionsFound = 0;

  $('#form-create-competencia #competencias_id-modal').children("option").each(function(i){
    iOptions++;

    if($.inArray($(this).val(), listCompetenciasPersona) > -1) {
      iOptionsFound++;

      $(this).hide();
      if ($(this).siblings(':not([style*=none])').length > 1) {
        $(this).siblings(':not(:selected)').prop('selected', true);
      }else {
        $(this).siblings(':not(:selected)').eq(1).prop('selected', true);
      }

      //console.log($('#form-create-competencia #competencias_id-modal option:not(:hidden)').length);
      //$('#form-create-competencia #competencias_id-modal option:not(:selected)').eq(0).prop('selected', true);
    }
  });

  if (iOptions == iOptionsFound) {
    $('#form-create-competencia #competencias_id-modal').hide();
    $('#form-create-competencia .wrp-competencias').append('<small class="text-danger" id="msg-no_more_competitions">No existen más competencias</small>');
    $('#form-create-competencia #btn-new-competencias').attr('disabled', true);
  }


}

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ---------------------- CREAR NUEVA COMPETENCIA ------------------------
// --------------------------------------------------------------------------
$("#form-create-competencia").on('submit', function(e){
  e.preventDefault();
  var form = document.getElementById("form-create-competencia");
  if (form.reportValidity()) {
    $.ajax({
      type: 'POST',
      url: $('#form-create-competencia').attr('action'),
      data: new FormData(form),
      contentType: false,
      cache: false,
      processData:false,
      beforeSend: function(){
        $('#form-create-competencia .loading').show();
      },
      success: function(data){
        console.log(JSON.parse(data));
        try {
          var dataJson = JSON.parse(data);
          //$('#form-create-competencia input[name="csrf_test_name"]').val(dataJson.update_token_ajax.csrf_hash);
          $('#msg-new-competencias').html('<br><h4 class="text-success">' + dataJson.msg + '</h4>').fadeIn('slow');
          setTimeout(function(){ $('#modal-create-competencia').modal('toggle'); }, 1500);
          tbl.draw();
          editRow($('#personal_id-modal').val());
        } catch (e) {
          $('#msg-new-competencias').html(renderMsgError('Error', 'Forbidden tryCatch')).fadeIn('slow');
        }
      },   
      complete: function(){
        $('#form-create-competencia .loading').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $('#msg-new-competencias').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
        //alert("Status: " + textStatus + "Error: " + errorThrown); 
      }
    });
  }
});


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ------------------- SETEAR DATOS EDITAR COMPETENCIA --------------------------
// --------------------------------------------------------------------------
function editCompetencia(id, id_personal) {  
  clearInputs();

  // Get Informacion de competencia de persona
  $.ajax({
    type: 'POST',
    url: '/personal/get-edit-competencia',
    data: {
      id: id,
    },
    beforeSend: function(){
      //$(this).siblings('.loading').show();      
    },
    success: function(data){
      var dataJ = JSON.parse(data);
      console.log(dataJ);
      $('#modal-edit-competencia .modal-title').text(dataJ.name_competencia);
      $('#modal-edit-competencia #competencias_id-modal option[value="' + dataJ.competencias_id + '"]').attr("selected",true);


      $('#modal-edit-competencia #competencias_id_hidden-modal').val(dataJ.competencias_id);

      $('#modal-edit-competencia #competencias_id-modal').attr('disabled', true);

      $('#modal-edit-competencia #expedition_date-modal').val(dataJ.expedition_date);
      $('#modal-edit-competencia #expiration_date-modal').val(dataJ.expiration_date);
      $('#modal-edit-competencia #validity_time-modal').val(dataJ.validity_time);

      if (dataJ.attached_certificate == null || dataJ.attached_certificate == '') {
        $('#modal-edit-competencia #current_certificado').attr('href','#');
        $('#modal-edit-competencia #current_certificado').attr('target','');
        $('#modal-edit-competencia #current_certificado').text('No disponible, cargue uno');
        $('#modal-edit-competencia #attached_certificate_hidden').val('');
      }else {
        $('#modal-edit-competencia #current_certificado').attr('href','/personal/download-file/' + dataJ.attached_certificate);
        $('#modal-edit-competencia #current_certificado').text('Ver');
        $('#modal-edit-competencia #attached_certificate_hidden-modal').val(dataJ.attached_certificate);
      }

      if (dataJ.active == 1) {
        $('#modal-edit-competencia #active-modal').prop('checked', true);
      }else {
        $('#modal-edit-competencia #active-modal').prop('checked', false);
      }

      
      $('#modal-edit-competencia #comment_report-modal').val(dataJ.comment_report);
      $('#modal-edit-competencia #id-modal').val(dataJ.id);  
      $('#modal-edit-competencia #personal_id-modal-competencia').val(id_personal);  

      



    },   
    complete: function(){
      //$(this).siblings('.loading').hide();
    },
    error: function(XMLHttpRequest, textStatus, errorThrown) {
      //$('#msg-update').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
      //alert("Status: " + textStatus + "Error: " + errorThrown); 
    }
  });
}

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ---------------------- EDITAR LA COMPETENCIA SELECCIONADA ------------------------
// --------------------------------------------------------------------------
$("#form-edit-competencia").on('submit', function(e){
  e.preventDefault();
  var form = document.getElementById("form-edit-competencia");
  if (form.reportValidity()) {
    $.ajax({
      type: 'POST',
      url: $('#form-edit-competencia').attr('action'),
      data: new FormData(this),
      contentType: false,
      cache: false,
      processData:false,
      beforeSend: function(){
        $('#form-edit-competencia .loading').show();
      },
      success: function(data){
        try {
          var dataJson = JSON.parse(data);
          console.log(dataJson);

          //dataJ.attached_certificate = 
          //$('#form-edit-competencia input[name="csrf_test_name"]').val(dataJson.update_token_ajax.csrf_hash);
          $('#msg-edit-competencias').html('<br><h4 class="text-success">' + dataJson.msg + '</h4>').fadeIn('slow');
          setTimeout(function(){ $('#modal-edit-competencia').modal('toggle'); }, 2000);
        } catch (e) {
          $('#msg-edit-competencias').html(renderMsgError('Error', 'Forbidden tryCatch')).fadeIn('slow');
        }
      },   
      complete: function(){
        $('#form-edit-competencia .loading').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $('#msg-edit-competencias').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
        //alert("Status: " + textStatus + "Error: " + errorThrown); 
      }
    });
  }
});


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// -------------------------- ELIMINAR COMPETENCIA ------------------------------
// --------------------------------------------------------------------------
$('.page-list-' + str_URL_1 + ' #form-delete-competencia').on('click', '#btn-delete-competencia', function() {
  //$('#msg-update-security-user').html('');
  console.log($('#personal_id-modal').val());
  var check = confirm(str_check);
  if(check === true) {
    $.ajax({
      type: 'POST',
      url: $('#form-delete-competencia').attr('action'),
      data: {
        id: $('#modal-edit-competencia #id-modal').val(),
        //csrf_test_name: $('#form-edit input[name="csrf_test_name"]').val(),
      },
      beforeSend: function(){
        $('#form-delete-competencia .loading').show();      
      },
      success: function(data){
        console.log(data);
        try {
          var dataJson = JSON.parse(data);
          console.log(dataJson);
          //$('#form-edit input[name="csrf_test_name"]').val(dataJson.update_token_ajax.csrf_hash);
          editRow($('#modal-edit-competencia #personal_id-modal-competencia').val());
          $('#modal-edit-competencia').modal('hide');
          $('#form-delete-competencia #msg-delete').html(dataJson.msg).fadeIn('slow');
        } catch (e) {
          $('#form-delete-competencia #msg-delete').html(renderMsgError('Error', 'Forbidden tryCatch')).fadeIn('slow');
        }
      },   
      complete: function(){
        $('#form-delete-competencia .loading').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $('#form-delete-competencia #msg-delete').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
      }
    });
  }
});



/*  
//file type validation
$("#attached_certificate-modal").change(function() {
  var file = this.files[0];
  var imagefile = file.type;
  console.log(imagefile);

  //application/pdf
  //application/vnd.openxmlformats-officedocument.wordprocessingml.document
  var match= ["image/jpeg","image/png","image/jpg"];
  if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2]))){
      alert('Please select a valid image file (JPEG/JPG/PNG).');
      $("#file").val('');
      return false;
  }

});
*/

/*
$('.page-list-' + str_URL_1).on('click', '#btn-new-competencias', function() {

   $('#msg-new-competencias').html('');
  var formData = new FormData('form-create-competencia');

  console.log(formData);

  return false;
  
  var form = document.getElementById("form-create-competencia");
  if (form.reportValidity()) {
    $.ajax({
      type: 'POST',
      url: $('#form-create-competencia').attr('action'),
      data: $('#form-create-competencia').serialize(),
      beforeSend: function(){
        $(this).siblings('.loading').show();      
      },
      success: function(data){
        try {
          var dataJson = JSON.parse(data);
          $('#form-edit input[name="csrf_test_name"]').val(dataJson.update_token_ajax.csrf_hash);
          $('#msg-new-competencias').html(dataJson.msg).fadeIn('slow');
          tbl.draw();
        } catch (e) {
          $('#msg-new-competencias').html(renderMsgError('Error', 'Forbidden tryCatch')).fadeIn('slow');
        }
      },   
      complete: function(){
        $(this).siblings('.loading').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $('#msg-new-competencias').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
        //alert("Status: " + textStatus + "Error: " + errorThrown); 
      }
    });
  }
});

*/




// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ---------------------- ACTUALIZAR INFO ------------------------
// --------------------------------------------------------------------------
$('.page-list-' + str_URL_1).on('click', '#btn-edit', function() {
  $('#msg-update').html('');
  
  var form = document.getElementById("form-edit");
  if (form.reportValidity()) {
    $.ajax({
      type: 'POST',
      url: $('#form-edit').attr('action'),
      data: new FormData(form),
      contentType: false,
      cache: false,
      processData:false,
      beforeSend: function(){
        $('#form-edit .loading').show();      
      },
      success: function(data){
        try {
          var dataJson = JSON.parse(data);

          $('#img-current').html('<br><b>Imagen Actual: </b><img width="45" height="45" src="./application/public/src/img-personal/' + dataJson.img_change + '">')
          $('#img_current_hidden-modal').val(dataJson.img_change);  

          $('#form-edit input[name="csrf_test_name"]').val(dataJson.update_token_ajax.csrf_hash);
          $('#msg-update').html(dataJson.msg).fadeIn('slow');
          tbl.draw();
        } catch (e) {
          $('#msg-update').html(renderMsgError('Error', 'Forbidden tryCatch')).fadeIn('slow');
        }
      },   
      complete: function(){
        $('#form-edit .loading').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $('#msg-update').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
        //alert("Status: " + textStatus + "Error: " + errorThrown); 
      }
    });
  }
});



// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// -------------------------- ELIMINAR ------------------------------
// --------------------------------------------------------------------------
$('.page-list-' + str_URL_1).on('click', '#btn-delete', function() {
  //$('#msg-update-security-user').html('');

  var check = confirm(str_check);
      
  if(check === true) {
    $.ajax({
      type: 'POST',
      url: $('#form-delete').attr('action'),
      data: {
        id: $('#id-modal').val(),
        csrf_test_name: $('#form-edit input[name="csrf_test_name"]').val(),
      },
      beforeSend: function(){
        $(this).siblings('.loading').show();      
      },
      success: function(data){
        try {
          tbl.draw();
          var dataJson = JSON.parse(data);
          $('#form-edit input[name="csrf_test_name"]').val(dataJson.update_token_ajax.csrf_hash);
          $('#modal-edit').modal('hide');
          $('.msg-ajax').html(dataJson.msg).fadeIn('slow');
        } catch (e) {
          $('#msg-delete').html(renderMsgError('Error', 'Forbidden tryCatch')).fadeIn('slow');
        }
      },   
      complete: function(){
        $(this).siblings('.loading').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $('#msg-delete').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
      }
    });
  }
});


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ------------------------------ CREAR  ------------------------------------
// --------------------------------------------------------------------------
$('.page-list-' + str_URL_1).on('click', '#btn-create', function() {

  var form = document.getElementById("form-create");
  if (form.reportValidity()) {
    $.ajax({
      type: 'POST',
      url: $('#form-create').attr('action'),
      data: new FormData(form),
      contentType: false,
      cache: false,
      processData:false,
      beforeSend: function(){
        $('#form-create .loading').show();      
      },
      success: function(data){
        console.log(data);
        try {
          var dataJson = JSON.parse(data);
          if (dataJson.ok) {
            window.location.href = "/" + str_URL_1;
          }else {
            $('#form-create input[name="csrf_test_name"]').val(dataJson.update_token_ajax.csrf_hash);
            //$('#modal-create').modal('hide');
            $('#msg-create').html(dataJson.msg).fadeIn('slow');
          }

          
        }catch (e) {
          $('#msg-create').html(renderMsgError('Error', 'Forbidden tryCatch')).fadeIn('slow');
        }
      },   
      complete: function(){
        $('#form-create .loading').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $('#msg-create').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
      }
    });
  }
});


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ----------------- RENDERIZAR MSG DE ERRORES AJAX -------------------------
// --------------------------------------------------------------------------
function renderMsgError(textStatus, errorThrown) {
  return '<div>' +
            '<p class="text-danger">' + str_access_denied + '</p>'+
            '<button type="button" class="btn btn-link" data-toggle="collapse" data-target="#show-more-msg">' + str_show_more + '</button>' +
            '<div id="show-more-msg" class="collapse">' +
              '<p>Status: ' + textStatus + '</p>' +
              '<p>Error: ' + errorThrown + '</p>' +
            '</div>'
          '</div>';
}

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ---------------------- LIMPIAR IMPUTS DE MODALS --------------------------
// --------------------------------------------------------------------------
function clearInputs(opc) {  
  // datos de usuario

  switch(opc) {
    case 'edit-competencia':
        $("#form-edit-competencia")[0].reset();
        break;
    default:
      $("#form-edit")[0].reset();
      $("#form-edit-competencias")[0].reset();
      $("#form-create")[0].reset();
      $("#form-create-competencia")[0].reset();
      

      $('#new-competencia').attr("data-personal-id", '');
      $('#new-competencia').attr("data-personal-name", '');
}

  
}


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// --------- OBTENER DIFERENCIA DE DIAS ENTRE 2 FECHAS ----------------------
// --------------------------------------------------------------------------
getDifferenceDays = function(f1,f2) {
  var aFecha1 = f1.split('-'); 
  var aFecha2 = f2.split('-'); 
  var fFecha1 = Date.UTC(aFecha1[0],aFecha1[1]-1,aFecha1[2]); 
  var fFecha2 = Date.UTC(aFecha2[0],aFecha2[1]-1,aFecha2[2]); 
  var dif = fFecha2 - fFecha1;
  var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
  
  return dias;
}


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// -------------------------- OBTENER FECHA DE HOY --------------------------
// --------------------------------------------------------------------------
function getToday() {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();

  if(dd<10) {
    dd = '0'+dd
  } 

  if(mm<10) {
    mm = '0'+mm
  } 

  return yyyy + '-' + mm + '-' + dd;
}


