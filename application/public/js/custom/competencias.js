var dataOBJ = {},
    tbl = null;

$(function() {
  if ($("#tbl-" + str_MODULE).length > 0) {

    // --------------------------------------------------------------------------
    // --------------------------------------------------------------------------
    // ---------------------------- TABLA ------------------------------
    // --------------------------------------------------------------------------
    tbl = $('#tbl-' + str_MODULE).DataTable({
      'paging': true,
      'info': true,
      'filter': true,
      'stateSave': true,
      'processing': true, 
      'serverSide': true,
      'lengthMenu' : [[10, 30, 50, -1], [10, 30, 50, 'Todo']], // edita el menu de cantidad de registros que se muestran
      dom: 'Bfrtlip',
      buttons: [
          {
              extend: 'excelHtml5',
              text: str_exportExcel,
              title: str_exportTitle,
              exportOptions: {
                  columns: [0, 1, 2, 3]
              }
          },
          {
              extend: 'pdfHtml5',
              text: str_exportPdf,
              title: str_exportTitle,
              exportOptions: {
                  columns: [0, 1, 2, 3]
              }
          }
      ],
      'ajax': {
        'url': '/' + str_URL_1 + '/ssp-list',
        'type': 'POST',
        'data': function (d) {
          return $.extend({}, d, {
            'dateStart': $('#dateStart').val(),
            'dateEnd': $('#dateEnd').val(),
          } );
        },
      },
      'columns': [
        {data: 'id', 'sClass': 'dt-body-center'},
        {data: 'name', 'sClass': 'dt-body-center'},
        {data: 'description', 'sClass': 'dt-body-center'},
        {'orderable': true,
          render:function(data, type, row) {
            dataOBJ[row.id] = row;

            return '<a href="#" data-toggle="modal" data-target="#modal-edit" onclick="editRow(\'' + row.id+ '\');">' +
                      '<i class="fa fa-cog fa-2x" aria-hidden="true"></i>' +
                    '</a>';       
          },
          'sClass': 'dt-body-center'
        }
      ],
       "oLanguage": {
          "sProcessing":     oLanguage.sProcessing,
          "sLengthMenu":     oLanguage.sLengthMenu,
          "sZeroRecords":    oLanguage.sZeroRecords,
          "sEmptyTable":     oLanguage.sEmptyTable,
          "sInfo":           oLanguage.sInfo,
          "sInfoEmpty":      oLanguage.sInfoEmpty,
          "sInfoFiltered":   oLanguage.sInfoFiltered,
          "sInfoPostFix":    "",
          "sSearch":         oLanguage.sSearch,
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": oLanguage.sLoadingRecords,
          "oPaginate": {
              "sFirst":    oLanguage.oPaginate.sFirst,
              "sLast":     oLanguage.oPaginate.sLast,
              "sNext":     oLanguage.oPaginate.sNext,
              "sPrevious": oLanguage.oPaginate.sPrevious
          },
          "oAria": {
              "sSortAscending": oLanguage.oAria.sSortAscending,
              "sSortDescending": oLanguage.oAria.sSortDescending
          }
      },
      "language": {
        "url": "dataTables.german.lang"
      },
      'order': [[0, 'desc']],
      'columnDefs': [
        {
          'targets': [1],
          'data': 'name',
          'render': function(data, type, row) {
            var DifferenceDays = getDifferenceDays(row.created_on_short, getToday());
            var newRow = (DifferenceDays == 0 || DifferenceDays == 1) ? '<small class="label label-success pull-left">New</small>' : '';
            return newRow + ' ' + row.name;
          },
        },
      ],
      /*"initComplete": function(settings, json) {
        //console.log(json.tokenAjax.csrf_hash);
        // Regenerar token csrf para nueva peticion ajax
        // nombre: json.tokenAjax.csrf_name
        $('#token-ajax').val(json.tokenAjax.csrf_hash);
      },*/
      
    });

    $("#tbl-" + str_MODULE + "_filter").append('<label>' +
          '<span style="margin-left:5px;">' + str_start + '</span>' +
          '<input type="text" id="dateStart" name="dateStart" readonly="true">' +
        '</label>' +
        '<label>' +
          '<span style="margin-left:5px;">' + str_end + '</span>' +
          '<input type="text" id="dateEnd" name="dateEnd" readonly="true">' +
        '</label>');

  
    $("#dateStart, #dateEnd").datepicker({
      changeMonth: true,
      changeYear: true,
      regional: 'es',
      dateFormat: 'yy-mm-dd',
      showButtonPanel: true,
      closeText: str_closeText,
      onClose: function () {
        var event = arguments.callee.caller.caller.arguments[0];

        if ($(event.delegateTarget).hasClass('ui-datepicker-close')) {
          $("#dateStart").val('');
          $("#dateEnd").val('');
          tbl.draw();
        }
      }
    });

    $('#dateStart, #dateEnd').change(function () {
      if ($("#dateStart").val() != "" && $("#dateEnd").val() != "") {
        tbl.draw();
      }
    });
  }// endif table-users
});


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ------------------- SETEAR DATOS EDITAR --------------------------
// --------------------------------------------------------------------------
function editRow(id) {  
  clearInputs();

  $('#id-modal').val(id);
  $('#name-modal').val(dataOBJ[id].name);
  $('#description-modal').val(dataOBJ[id].description);
  
  if (dataOBJ[id].active == 1) {
    $('#active-modal').prop('checked', true);
  }else {
    $('#active-modal').prop('checked', false);
  }

  if (dataOBJ[id].img != null && dataOBJ[id].img != '') {
    $('#img-current').html('<br><b>Imagen Actual: </b><img width="45" height="45" src="./application/public/src/img-competencias/' + dataOBJ[id].img + '">')

    $('#img_current_hidden-modal').val(dataOBJ[id].img);
  }else {
    $('#img-current').html('<small>No ha cargado ningun logo</small>')
    $('#img_current_hidden-modal').val('');
  }
}


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ---------------------- ACTUALIZAR INFO ------------------------
// --------------------------------------------------------------------------
$('.page-list-' + str_URL_1).on('click', '#btn-edit', function() {
  $('#msg-update').html('');
  
  var form = document.getElementById("form-edit");
  if (form.reportValidity()) {
    $.ajax({
      type: 'POST',
      url: $('#form-edit').attr('action'),
      data: new FormData(form),
      contentType: false,
      cache: false,
      processData:false,
      beforeSend: function(){
        $('#form-edit .loading').show();      
      },
      success: function(data){
        try {
          var dataJson = JSON.parse(data);
          $('#img-current').html('<br><b>Imagen Actual: </b><img width="50" height="50" src="./application/public/src/img-competencias/' + dataJson.img_change + '">')
          $('#img_current_hidden-modal').val(dataJson.img_change);  


          $('#form-edit input[name="csrf_test_name"]').val(dataJson.update_token_ajax.csrf_hash);
          $('#msg-update').html(dataJson.msg).fadeIn('slow');
          tbl.draw();
        } catch (e) {
          $('#msg-update').html(renderMsgError('Error', 'Forbidden tryCatch')).fadeIn('slow');
        }
      },   
      complete: function(){
        $('#form-edit .loading').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $('#msg-update').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
        //alert("Status: " + textStatus + "Error: " + errorThrown); 
      }
    });
  }
});



// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// -------------------------- ELIMINAR ------------------------------
// --------------------------------------------------------------------------
$('.page-list-' + str_URL_1).on('click', '#btn-delete', function() {
  //$('#msg-update-security-user').html('');

  var check = confirm(str_check);
      
  if(check === true) {
    $.ajax({
      type: 'POST',
      url: $('#form-delete').attr('action'),
      data: {
        id: $('#id-modal').val(),
        csrf_test_name: $('#form-edit input[name="csrf_test_name"]').val(),
      },
      beforeSend: function(){
        $(this).siblings('.loading').show();      
      },
      success: function(data){
        try {
          tbl.draw();
          var dataJson = JSON.parse(data);
          $('#form-edit input[name="csrf_test_name"]').val(dataJson.update_token_ajax.csrf_hash);
          $('#modal-edit').modal('hide');
          $('.msg-ajax').html(dataJson.msg).fadeIn('slow');
        } catch (e) {
          $('#msg-delete').html(renderMsgError('Error', 'Forbidden tryCatch')).fadeIn('slow');
        }
      },   
      complete: function(){
        $(this).siblings('.loading').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $('#msg-delete').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
      }
    });
  }
});


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ------------------------------ CREAR  ------------------------------------
// --------------------------------------------------------------------------
$('.page-list-' + str_URL_1).on('click', '#btn-create', function() {

  var form = document.getElementById("form-create");
  if (form.reportValidity()) {
    $.ajax({
      type: 'POST',
      url: $('#form-create').attr('action'),
      data: new FormData(form),
      contentType: false,
      cache: false,
      processData:false,
      beforeSend: function(){
        $('#form-create .loading').show();      
      },
      success: function(data){
        console.log(data);
        try {
          var dataJson = JSON.parse(data);
          if (dataJson.ok) {
            window.location.href = "/" + str_URL_1;
          }else {
            $('#form-create input[name="csrf_test_name"]').val(dataJson.update_token_ajax.csrf_hash);
            //$('#modal-create').modal('hide');
            $('#msg-create').html(dataJson.msg).fadeIn('slow');
          }

          
        }catch (e) {
          $('#msg-create').html(renderMsgError('Error', 'Forbidden tryCatch')).fadeIn('slow');
        }
      },   
      complete: function(){
        $('#form-create .loading').hide();
      },
      error: function(XMLHttpRequest, textStatus, errorThrown) {
        $('#msg-create').html(renderMsgError(textStatus, errorThrown)).fadeIn('slow');
      }
    });
  }
});


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ----------------- RENDERIZAR MSG DE ERRORES AJAX -------------------------
// --------------------------------------------------------------------------
function renderMsgError(textStatus, errorThrown) {
  return '<div>' +
            '<p class="text-danger">' + str_access_denied + '</p>'+
            '<button type="button" class="btn btn-link" data-toggle="collapse" data-target="#show-more-msg">' + str_show_more + '</button>' +
            '<div id="show-more-msg" class="collapse">' +
              '<p>Status: ' + textStatus + '</p>' +
              '<p>Error: ' + errorThrown + '</p>' +
            '</div>'
          '</div>';
}

// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// ---------------------- LIMPIAR IMPUTS DE MODALS --------------------------
// --------------------------------------------------------------------------
function clearInputs() {  
  // datos de usuario
  $("#form-edit")[0].reset();
  $("#form-create")[0].reset();
}


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// --------- OBTENER DIFERENCIA DE DIAS ENTRE 2 FECHAS ----------------------
// --------------------------------------------------------------------------
getDifferenceDays = function(f1,f2) {
  var aFecha1 = f1.split('-'); 
  var aFecha2 = f2.split('-'); 
  var fFecha1 = Date.UTC(aFecha1[0],aFecha1[1]-1,aFecha1[2]); 
  var fFecha2 = Date.UTC(aFecha2[0],aFecha2[1]-1,aFecha2[2]); 
  var dif = fFecha2 - fFecha1;
  var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
  
  return dias;
}


// --------------------------------------------------------------------------
// --------------------------------------------------------------------------
// -------------------------- OBTENER FECHA DE HOY --------------------------
// --------------------------------------------------------------------------
function getToday() {
  var today = new Date();
  var dd = today.getDate();
  var mm = today.getMonth()+1; //January is 0!
  var yyyy = today.getFullYear();

  if(dd<10) {
    dd = '0'+dd
  } 

  if(mm<10) {
    mm = '0'+mm
  } 

  return yyyy + '-' + mm + '-' + dd;
}


