<!-- Modal Editar Empresa -->
<div id="modal-create" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-blue">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= _('New'); ?></h4>
      </div>

      <div class="wrp-tabs"> 
        <ul class="nav nav-tabs">
          <li class="active">
            <a  href="#tab-create" data-toggle="tab"><i class="fa fa-user" aria-hidden="true"></i> <?= _('Imformation'); ?></a>
          </li>
        </ul>

          <div class="tab-content ">
            <!-- tab create -->
            <div class="tab-pane active" id="tab-create">
              <?php
                $form_attr = array('id' => 'form-create', 'class' => 'form-horizontal');
                $label_attr = array('class' => 'col-sm-3 control-label');
              ?>
              <?= form_open($URL_1 . '/create', $form_attr); ?>
                <div class="modal-body">
                  <div class="box-body">
                    <div class="row">
                      <br>
                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Name'), 'name', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'name-modal',
                                'name' => 'name', 
                                'placeholder' => _('Enter the name'),
                                'class' => 'form-control',
                                'required' => 'required',
                                'maxlength' => 45,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Description'), 'description', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'description-modal',
                                'name' => 'description', 
                                'placeholder' => _('Enter the description'),
                                'class' => 'form-control',
                                'maxlength' => 250,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Contract Number'), 'contract_number', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'contract_number-modal',
                                'name' => 'contract_number', 
                                'placeholder' => _('Enter the contract number'),
                                'class' => 'form-control',
                                'required' => 'required',
                                'maxlength' => 45,
                              );
                              ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Address'), 'address', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'address-modal',
                                'name' => 'address', 
                                'placeholder' => _('Enter the address'),
                                'class' => 'form-control',
                                'maxlength' => 45,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Phone'), 'phone', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'phone-modal',
                                'name' => 'phone', 
                                'placeholder' => _('Enter the phone'),
                                'maxlength' => 30,
                                'class' => 'form-control',
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Email'), 'email', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'email-modal',
                                'name' => 'email', 
                                'class' => 'form-control',
                                'type' => 'email',
                                'maxlength' => 45,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Active'), 'active', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'active-modal',
                                'name' => 'active',
                              );
                            ?>
                            <?= form_checkbox($input, 1, NULL); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Image'), 'img', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'img-modal',
                                'name' => 'file', 
                                'class' => 'form-control',
                                'type' => 'file',
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <?php
                        $input = array(
                          'id'    => 'id-modal',
                          'type'  => 'hidden',
                          'name'  => 'id',
                        );
                      ?>
                      <?= form_input($input); ?>
                    </div>  
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="group-button text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ><?= _('Close'); ?></button>
                    <?php if($this->my_acl->access_control(FALSE, $MODULE, $CONTROL, 'create_ajax')) : ?>
                      <input type="submit" id="btn-create" name="btn-create" value="<?= _('Save'); ?>" class="btn btn-primary">
                      <div class="loading text-center" style="display: none;">
                        <br>
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <br>
                      </div>
                    <?php endif; ?>
                  </div>
                  <div id="msg-update" class="text-center" style="display: none;">
                    <!-- Muestra mensajes ajax -->
                  </div>
                </div>
              <?= form_close(); ?>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>