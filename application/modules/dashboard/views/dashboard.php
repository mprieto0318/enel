<div class="col-md-12">
	
  <div class="row">

    <div class="panel panel-default">
      <!--<div class="panel-heading">Panel Header</div>-->
      <div class="panel-body">
        <!-- USUARIOS -->
        <?php if($this->my_acl->access_control(FALSE, 'user', 'Admin', 'index')) : ?>
          <div class="col-md-3">
            <div class="panel panel-danger">
              <a href="user/list-of-users">
                <div class="panel-heading">
                  <div class="row">
                    <div class="col-xs-6">
                      <i class="fa fa-user fa-5x text-danger"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                      <p class="announcement-heading"><span class="glyphicon glyphicon-pushpin"></span></p>
                      <!-- <p class="announcement-text"></p> -->
                    </div>
                  </div>
                </div>
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      <?= _('Users'); ?>
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        <?php endif; ?>

        <!-- EMPRESAS COLABORADORAS -->
        <?php if($this->my_acl->access_control(FALSE, 'empresascol', 'Empresascol', 'index')) : ?>
          <div class="col-md-3">
            <div class="panel panel-info">
              <a href="/empresas-colaboradoras">
                <div class="panel-heading">
                  <div class="row">
                    <div class="col-xs-6">
                      <i class="fa fa-building-o fa-5x text-info"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                      <p class="announcement-heading"><span class="glyphicon glyphicon-pushpin"></span></p>
                      <!-- <p class="announcement-text"></p> -->
                    </div>
                  </div>
                </div>

                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-10">
                      <?= _('Collaborating Companies'); ?>
                    </div>
                    <div class="col-xs-2 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        <?php endif; ?>

        <!-- CARGOS -->
        <?php if($this->my_acl->access_control(FALSE, 'cargos', 'Cargos', 'index')) : ?>
          <div class="col-md-3">
            <div class="panel panel-warning">
              <a href="/cargos">
                <div class="panel-heading">
                  <div class="row">
                    <div class="col-xs-6">
                      <i class="fa fa-id-badge fa-5x text-warning"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                      <p class="announcement-heading"><span class="glyphicon glyphicon-pushpin"></span></p>
                      <!-- <p class="announcement-text"> Historial Saldo</p> -->
                    </div>
                  </div>
                </div>

                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-10">
                      <?= _('Appointment'); ?>
                    </div>
                    <div class="col-xs-2 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        <?php endif; ?>
        
        <!-- NIVEL ACADEMICO -->
        <?php if($this->my_acl->access_control(FALSE, 'nivelacademico', 'Nivelacademico', 'index')) : ?>
          <div class="col-md-3">
            <div class="panel panel-success">
              <a href="/nivel-academico">
                <div class="panel-heading">
                  <div class="row">
                    <div class="col-xs-6">
                      <i class="fa fa-child fa-5x text-success"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                      <p class="announcement-heading"><span class="glyphicon glyphicon-pushpin"></span></p>
                      <!--<p class="announcement-text"> Disponible</p>-->
                    </div>
                  </div>
                </div>
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-10">
                      Academic level
                    </div>
                    <div class="col-xs-2 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        <?php endif; ?>

        <!-- COMPETENCIAS -->
        <?php if($this->my_acl->access_control(FALSE, 'competencias', 'Competencias', 'index')) : ?>
          <div class="col-md-3">
            <div class="panel panel-default">
              <a href="/competencias">
                <div class="panel-heading">
                  <div class="row">
                    <div class="col-xs-6">
                      <i class="fa fa-address-card-o fa-5x text-default"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                      <p class="announcement-heading"><span class="glyphicon glyphicon-pushpin"></span></p>
                      <!-- <p class="announcement-text"></p> -->
                    </div>
                  </div>
                </div>
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      <?= _('Competitions'); ?>
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        <?php endif; ?>

        <!-- PERSONAL -->
        <?php if($this->my_acl->access_control(FALSE, 'personal', 'Personal', 'index')) : ?>
          <div class="col-md-3">
            <div class="panel panel-danger">
              <a href="/personal">
                <div class="panel-heading">
                  <div class="row">
                    <div class="col-xs-6">
                      <i class="fa fa-users fa-5x text-danger"></i>
                    </div>
                    <div class="col-xs-6 text-right">
                      <p class="announcement-heading"><span class="glyphicon glyphicon-pushpin"></span></p>
                      <!-- <p class="announcement-text"></p> -->
                    </div>
                  </div>
                </div>
                <div class="panel-footer announcement-bottom">
                  <div class="row">
                    <div class="col-xs-6">
                      <?= _('Personal'); ?>
                    </div>
                    <div class="col-xs-6 text-right">
                      <i class="fa fa-arrow-circle-right"></i>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        <?php endif; ?>

      </div>
    </div>

  </div>

</div>
