<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Dashboard extends MX_Controller {
 
	public function __construct() {
    parent::__construct();

    /*if(!validate_session()) {
      redirect('user/login');
    }*/

    $this->load->model('Dashboard_model');
  }
 
  // Page Dashboard
  public function index() {

    $data = array(
      'title' => _('Dashboard'), 
      'show_title' => TRUE, 
      'page_render' => 'dashboard/dashboard',
    );

    $this->load->view('layout/template',$data);
  }
 
}