<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Nivelacademico extends MX_Controller {
 
	public function __construct() {
    parent::__construct();

    //if(!validate_session()) {
      //redirect('user/login');
    //}

    $this->load->model('Nivelacademico_model', 'GetModel');
  }
 
  // Page List of Users
  public function index() {
    // call helper
    add_css(datatables_css());
    add_js(datatables_js());
    add_js(array(base_url() . 'application/public/js/custom/' . $this->router->fetch_module() . '.js'));

    $data = array(
      'MODULE' => $this->router->fetch_module(),
      'CONTROL' => $this->router->fetch_class(),
      'URL_1' => $this->uri->segment(1),
      'title' => _('List of Academic Level'), 
      'show_title' => TRUE, 
      'page_render' => 'nivelacademico/list',
      'body_class' => 'page-list-' . $this->uri->segment(1),
    );

    $this->load->view('layout/template',$data);
  }

  function ssp_list() {
    if (!$this->input->is_ajax_request()) {
      exit('No direct script access allowed');
    }

    $inputs = $this->input->post();

    // Campos DataTable
    $draw = $this->input->post('draw');

    $result = $this->GetModel->ssp_list($inputs);

    $data = array();
    foreach ($result['data']->result() as $key => $value) {
      $data[$key]['id'] = $value->id;
      $data[$key]['name'] = $value->name;
      $data[$key]['description'] = $value->description;
      $data[$key]['created_on'] = $value->created_on;
      $explode_created = explode(' ', $value->created_on);
      $data[$key]['created_on_short'] = $explode_created[0];
    }

    $json_data = array(
      "draw"            => intval($draw),
      "recordsTotal"    => intval($result['total_data_filter']),
      "recordsFiltered" => intval($result['total_data']),
      "data"            => $data,
    );

    echo json_encode($json_data);
  }

  function update_ajax() {
    if (!$this->input->is_ajax_request()) {
      exit('No direct script access allowed');
    }

    $inputs = $this->input->post();

    $values = array(
      'name' => $inputs['name'], 
      'description' => $inputs['description'],
    );
    
    $result = $this->GetModel->update($inputs['id'], $values);

    //$msg = '<br><p class="text-danger">No se pudo actualizar el usuario intente mas tarde</p>';
    if ($result) {
      $msg = '<br><p class="text-success">' . _('Updated') . ' <i class="fa fa-check" aria-hidden="true"></i></p>';
    }else {
      $msg = '<br><p class="text-danger">' . _('Edit the content to update') . ' <i class="fa fa-exclamation" aria-hidden="true"></i></p>';
    }

    $update_token_ajax = array(
      'csrf_name' => $this->security->get_csrf_token_name(), 
      'csrf_hash' => $this->security->get_csrf_hash(), 
    );

    $json_data = array(
      'msg' => $msg, 
      'update_token_ajax' => $update_token_ajax, 
    );

    echo json_encode($json_data);
  }

  function delete_ajax() {
    if (!$this->input->is_ajax_request()) {
      exit('No direct script access allowed');
    }

    $result = $this->GetModel->delete('id', $this->input->post('id'));

    if ($result) {
      $msg = '<br><h4 class="text-success">' . _('Deleted') . ' <i class="fa fa-check" aria-hidden="true"></i></h4>';
    }else {
      $msg = '<br><h4 class="text-danger">' . _('The record could not be deleted') . '</h4>';
    }

    $update_token_ajax = array(
      'csrf_name' => $this->security->get_csrf_token_name(), 
      'csrf_hash' => $this->security->get_csrf_hash(), 
    );

    $json_data = array(
      'msg' => $msg, 
      'update_token_ajax' => $update_token_ajax, 
    );

    echo json_encode($json_data);
  }

  function create_ajax() {
    if (!$this->input->is_ajax_request()) {
      exit('No direct script access allowed');
    }

    $inputs = $this->input->post();

    $ok = TRUE;
    $type = '';
    $msg = '';
    $msg_ajax = '';

    $values = array(
      'name' => $inputs['name'], 
      'description' => $inputs['description'],
    );

    $result = $this->GetModel->create($values);

    if ($result) {
      $type = 'success';
      $msg = _('Created');
      save_messages($type, $msg);
    }else {
      $ok = FALSE;
      $msg_ajax = '<br><h4 class="text-danger">' . _('Could not create the record') . '</h4>';
    }
    
    $update_token_ajax = array(
      'csrf_name' => $this->security->get_csrf_token_name(), 
      'csrf_hash' => $this->security->get_csrf_hash(), 
    );

    $json_data = array(
      'ok' => $ok,
      'msg' => $msg_ajax, 
      'update_token_ajax' => $update_token_ajax, 
    );

    echo json_encode($json_data);
  }
}