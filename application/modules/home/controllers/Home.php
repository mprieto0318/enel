<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Home extends MX_Controller {
 
  public function __construct() {
    parent::__construct();
    $this->load->model('Home_model', 'GetModel');
  }
 
  public function index($cc = false) {
  	$data = array(
   		'title' => _('Search'), 
   		'show_title' => FALSE, 
   		'page_render' => 'home/index',
      'body_class' => 'page-' . $this->uri->segment(1),
   	);

    $uri_public = base_url() . 'application/public/';
    
    $js = array(
      $uri_public . 'js/custom/home.js',
    );

    add_js($js);

    if ($cc != false) {
      $type = 'danger';
      $msg = _('The person was not found in the system, check the document') . ' C.C. ' . $cc;
      save_messages($type, $msg);
    }

   	$this->load->view('layout/template',$data);
  }

  function search_byCC() {

    $cc = $this->input->post('document');

    $result = $this->GetModel->search_byCC($cc);

    if ($result == false) {
      // Retorna a interface de busqueda
      $this->index($cc);
    }else {
      
      $data = array(
        'title' => _('Person Found'), 
        'show_title' => FALSE, 
        'page_render' => 'home/busqueda-persona',
        'result' => $result,
        'body_class' => 'page-' . $this->uri->segment(1),
        'MODULE' => $this->router->fetch_module(),
        'CONTROL' => $this->router->fetch_class(),
        'URL_1' => $this->uri->segment(1),
      );

      $uri_public = base_url() . 'application/public/';
      
      $js = array(
        $uri_public . 'js/custom/home.js',
      );

      add_js($js);

      $this->load->view('layout/template',$data);
    }

  }

  function reportar_competencia() {

    $id = $this->input->post('id');
    $comment_report = $this->input->post('comment_report');

    $values = array(
      'comment_report' => $comment_report,
      'active' => 0,
    );

    $result = $this->GetModel->reportar_competencia($id, $values);

    if ($result) {
      $msg = '<br><p class="text-success">' . _('Reported') . ' <i class="fa fa-check" aria-hidden="true"></i></p>';
    }else {
      $msg = '<br><p class="text-danger">' . _('Edit the content to report') . ' <i class="fa fa-exclamation" aria-hidden="true"></i></p>';
    }

    $json_data = array(
      'msg' => $msg,
    );

    echo json_encode($json_data);
  }


}