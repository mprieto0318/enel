<?php
  $uri_public = base_url() . 'application/public/';
?>
<div id="modal-competencia" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-blue">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{nombre competencia}}</h4>
      </div>

      <div class="modal-body">
        <div class="box-body">
          <div class="row">
            <br>

            <div class="col-md-12 text-center">
              <!--<img id="img_competencia-modal" class="img-responsive" src="<?= $uri_public ;?>src/site/img/logo.png">-->
              <i class="fa fa-rocket fa-2x" aria-hidden="true"></i>
            </div>

            <div class="col-md-6 text-center">
              <p><b><?= _('Expedition date: '); ?></b> <span id="expedition_date"></span></p>
            </div>

            <div class="col-md-6 text-center">
              <p><b><?= _('Valid until: '); ?></b> <span id="expiration_date"></span></p>
            </div>

            <div class="col-md-12 text-center">
              <p><b><?= _('Validity: '); ?></b> <span id="validity_time"></span></p>
            </div>

            <div class="col-md-12 text-center">
              <a id="attached_certificate" href="" target="_blank">
                <img width="50" height="50" src="<?= $uri_public; ?>src/site/img/descargar-certificado.png"><br>
                
                <small><?= _('Certificate'); ?><p class="text-danger"><?= _('Not available'); ?></p></small>
              </a>
            </div>

            <hr>

            <div class="col-md-offset-1 col-md-10 text-center">
              <br><br>
              <label class="text-danger">Reportar Competencia:</label>
              <textarea class="form-control" id="comment_report"></textarea>
              <?php if($this->my_acl->access_control(FALSE, $MODULE, $CONTROL, 'reportar_competencia')) : ?>
                <br>
                <button id="submit-reportar" class="btn btn-danger">Reportar</button>
                <br>
                <div id="msg-reportar-competencia"></div>
              <?php endif; ?>
              <br>
              <br>
            </div>  
        </div>
      </div>
      <div class="modal-footer">
        <div class="group-button text-center">
          <button type="button" class="btn btn-default" data-dismiss="modal" ><?= _('Close'); ?></button>  
        </div>
        
      </div>
    </div>
  </div>
</div>