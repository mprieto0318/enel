<?php
  $uri_public = base_url() . 'application/public/';
?>

<div class="col-md-6 col-md-offset-3" id="wrp-busqueda-persona">
  <br>
  <a href="/busqueda" class="text-left"><i class="fa fa-chevron-left" aria-hidden="true"></i> <?= _('Search again'); ?></a>
  <br>
  
  <!-- Info Persona -->
  <div class="row">
    <div class="col-md-12 text-center">

      <?php
        $filePath = './application/public/src/img-empresas/' . $result['info']->img_empresa;
        if(is_file($filePath) && file_exists($filePath)) : ?>
          <img style="width: 40px; height: auto; margin: 0 auto;" id="logo-empresa" class="img-responsive" src="<?= $uri_public ;?>src/img-empresas/<?= $result['info']->img_empresa; ?>">
      <?php  else : ?>
          <i class="fa fa-building-o fa-3x" aria-hidden="true"></i>
      <?php endif; ?>

        
      <p id="nombre-empresa"><?= $result['info']->name_empresa; ?></p>
      <br>
      <?php
        $filePath = './application/public/src/img-personal/' . $result['info']->img;
        if(is_file($filePath) && file_exists($filePath)) : ?>
          <img style="width: 80px; height: 80px; margin: 0 auto;" id="img-persona" class="img-responsive img-circle" src="<?= $uri_public ;?>src/img-personal/<?= $result['info']->img; ?>">
      <?php  else : ?>
          <i class="fa fa-user-circle-o fa-5x" aria-hidden="true"></i>
      <?php endif; ?>
      

      <?php
        $full_name = $result['info']->name . ' ' . $result['info']->surname . ' ' . $result['info']->second_surname;
      ?>

      <p id="nombre-persona"><?= $full_name; ?></p>
      <p id="cc-persona"><?=  $result['info']->cc; ?></p>
    </div>
    <div class="col-md-12 text-center">
      <?= _('Authorized to perform the following tasks'); ?>:
      <br>
      <br>
    </div>
  </div>

  <!-- Competencias -->
  <div class="row">
  <?php foreach ($result['competencias'] as $key => $value) : ?>
    <div class="col-xs-4 col-md-4 text-center">
      <?php $class = 'text-success'; ?>
      <?php if (!$value->active) : ?>
        <?php  $class = 'text-danger'; ?>
        <small class="text-danger"><i class="fa fa-ban" aria-hidden="true"></i></small>
        <br>
      <?php else: ?>
        <small class="text-success"><i class="fa fa-check-square-o" aria-hidden="true"></i></small>
        <br>
      <?php endif; ?>
      <a data-competencia="<?= htmlspecialchars(json_encode($value)); ?>"  href="#" data-toggle="modal" data-target="#modal-competencia" class="ver-competencia <?= $class; ?>">
        <!--<img id="img-competencia" class="img-responsive" src="<?= $uri_public ;?>src/site/img/logo.png">-->

        <?php
        $filePath = './application/public/src/img-competencias/' . $value->img_competencia;
          if(is_file($filePath) && file_exists($filePath)) : ?>
            <img style="width: 31px; height: 31px; margin: 0 auto;" id="img-competencia" class="img-responsive" src="<?= $uri_public ;?>src/img-competencias/<?= $value->img_competencia; ?>">
        <?php  else : ?>
            <i class="fa fa-rocket fa-2x" aria-hidden="true"></i>
        <?php endif; ?>

        <p id="nombre-competencia"><?= $value->name_competencia; ?></p>
      </a>
    </div>
  <?php endforeach; ?>
  </div>

</div>

<?= $this->load->view('home/modals/competencia'); ?>
