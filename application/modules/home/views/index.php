<?php
  $uri_public = base_url() . 'application/public/';
?>
<div class="col-md-6 col-md-offset-3">
  <br>
  <br>
  <div class="panel panel-primary">
    
    <div class="panel-heading">
      <i class="fa fa-search" aria-hidden="true"></i> <?= _('Search'); ?>
    </div>

    <div class="panel-body">
      <?php
        $form_attr = array('id' => 'form-search', 'class' => 'form-horizontal');
        $label_attr = array('class' => 'col-sm-3 control-label');
      ?>
      <?= form_open('/busqueda-persona', $form_attr); ?>
          <div class="box-body">
            <div class="row">
              <br>
              <div class="col-md-12">
                <div class="form-group">
                  <?= form_label(_('Document'), 'document', $label_attr); ?>
                  <div class="col-sm-9">
                    <?php
                      $input = array(
                        'id' => 'document',
                        'name' => 'document', 
                        'placeholder' => _('Enter the document'),
                        'class' => 'form-control',
                        'required' => 'required',
                        'maxlength' => 20,
                      );
                    ?>
                    <?= form_input($input); ?>
                  </div>
                </div>
              </div>
            </div>  
          </div>
        
          <div class="group-button text-center">
            <input type="submit" id="btn-search" name="btn-search" value="<?= _('Search'); ?>" class="btn btn-primary">
            <!--<a href="#" id="trigger-modal-person" data-toggle="modal" data-target="#modal-person" style="display:none;"></a>-->
          </div>
      <?= form_close(); ?>
    </div>
  </div>
</div>
