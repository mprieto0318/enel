<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

  public $tbl;

  public function __construct() {
    parent::__construct();
  }

  function search_byCC($cc) {
    // Get Info Personal
    $selects = 'p.id, p.cc, p.surname, p.second_surname, p.name, p.start_contract_date, p.professional_registration, p.img, p.active, p.empresa_id, p.cargos_id, p.nivel_academico_id, p.created_on, e.name name_empresa, e.img img_empresa, c.name name_cargo, n.name name_nivel_academico';

    $info = $this->db->select($selects);
    $info->join('empresa e', 'e.id = p.empresa_id');
    $info->join('cargos c', 'c.id = p.cargos_id');
    $info->join('nivel_academico n', 'n.id = p.nivel_academico_id');
    $info->where('p.cc', $cc);
    $info = $info->get('personal p');

    

    // Get Competencias
    $selects = 'r.id, r.expedition_date, r.expiration_date, r.validity_time, r.attached_certificate, r.created_on, r.personal_id, r.competencias_id, r.comment_report, r.active, c.name name_competencia, c.img img_competencia';

    $compe = $this->db->select($selects);
    $compe->join('competencias c', 'c.id = r.competencias_id');
    $compe->join('personal p', 'p.id = r.personal_id');
    $compe->where('p.cc', $cc);
    $compe = $compe->get('relacion_competencia r');

    if (empty($info->result())) {
      return false;
    }

    $data = array(
      'info' => $info->result()[0],
      'competencias' => $compe->result(),
    );

    return $data;
    
  }


   function reportar_competencia($id, $values) {
    $this->db->where('id', $id);
    $this->db->update('relacion_competencia', $values);

    return $this->db->affected_rows();
  }
}