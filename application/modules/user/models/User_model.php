<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model {

  public function __construct() {
    parent::__construct();
  }

  function login($valuesEmail, $valuesUsername) {

    return $this->db->from('users')
            ->group_start()
              ->where($valuesEmail)
                ->or_group_start()
                        ->where($valuesUsername)
                ->group_end()
            ->group_end()
            ->get()->row();

    //$this->db->select('id, username, email')->where($values);
    //return $this->db->get('users')->row();
  }

  function update_last_login($id) {
    $this->db->update('users', array('last_login' => time()), array('id' => $id));

    return $this->db->affected_rows() == 1;
  }


  //function user_exists($values) {
    //$this->db->from('users')->where($values);
    
    //return ($this->db->get()->num_rows() > 0) ? TRUE : FALSE;
  //}
}