<!-- Modal Editar Usuario -->
<div id="modal-edit-user" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-blue">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= _('Edit User'); ?></h4>
      </div>

      <div class="wrp-tabs"> 
        <ul class="nav nav-tabs">
          <li class="active">
            <a  href="#tab-update-user" data-toggle="tab"><i class="fa fa-user" aria-hidden="true"></i> <?= _('Update'); ?></a>
          </li>
          <li><a href="#tab-security-user" data-toggle="tab"><i class="fa fa-shield" aria-hidden="true"></i> <?= _('Security'); ?></a>
          </li>
          <li><a href="#tab-delete-user" data-toggle="tab"><i class="fa fa-trash-o" aria-hidden="true"></i> <?= _('Delete'); ?></a>
          </li>
        </ul>

          <div class="tab-content ">
            <!-- tab update user -->
            <div class="tab-pane active" id="tab-update-user">
              <?php
                $form_attr = array('id' => 'form-edit-user', 'class' => 'form-horizontal');
                $label_attr = array('class' => 'col-sm-3 control-label');
              ?>
              <?= form_open('user/update-user', $form_attr); ?>
                <div class="modal-body">
                  <div class="box-body">
                    <div class="row">
                      <br>
                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Username'), 'username', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'username-modal',
                                'name' => 'username', 
                                'placeholder' => _('Enter the username'),
                                'class' => 'form-control',
                                'required' => 'required',
                                'maxlength' => 10,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Email'), 'email', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'email-modal',
                                'name' => 'email', 
                                'placeholder' => _('Enter the email'),
                                'class' => 'form-control',
                                'type' => 'email',
                                'required' => 'required',
                                'maxlength' => 50,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('First name'), 'first_name', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'first_name-modal',
                                'name' => 'first_name', 
                                'placeholder' => _('Enter the name'),
                                'class' => 'form-control',
                                'required' => 'required',
                                'maxlength' => 50,
                              );
                              ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Last name'), 'last_name', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'last_name-modal',
                                'name' => 'last_name', 
                                'placeholder' => _('Enter the surnames'),
                                'class' => 'form-control',
                                'required' => 'required',
                                'maxlength' => 50,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Phone'), 'phone', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'phone-modal',
                                'name' => 'phone', 
                                'placeholder' => _('Enter the phone'),
                                'maxlength' => 20,
                                'class' => 'form-control',
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Last login'), 'last-login', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'last_login-modal',
                                'name' => 'last_login', 
                                'class' => 'form-control',
                                'readonly' => 'readonly'
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Active'), 'active', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'active-modal',
                                'name' => 'active',
                              );
                            ?>
                            <?= form_checkbox($input, 1, NULL); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Role'), 'roles_id', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'roles_id-modal',
                                'name' => 'roles_id', 
                                'class' => 'form-control',
                                'options' => $roles,
                              );
                            ?>
                            <?= form_dropdown($input); ?>
                          </div>
                        </div>
                      </div>

                      <?php
                        $input = array(
                          'id'    => 'id-modal',
                          'type'  => 'hidden',
                          'name'  => 'id',
                        );
                      ?>
                      <?= form_input($input); ?>
                    </div>  
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="group-button text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ><?= _('Close'); ?></button>
                    <?php if($this->my_acl->access_control(FALSE, 'user', 'Admin', 'update_user_ajax')) : ?>
                      <input type="button" id="save-edit-user" name="save-edit-user" value="<?= _('Save'); ?>" class="btn btn-primary">
                      <div class="loading text-center" style="display: none;">
                        <br>
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <br>
                      </div>
                    <?php endif; ?>
                  </div>
                  <div id="msg-update-user" class="text-center" style="display: none;">
                    <!-- Muestra mensajes ajax -->
                  </div>
                </div>
              <?= form_close(); ?>
            </div>

            <!-- tab security user -->
            <div class="tab-pane" id="tab-security-user">
              <?php
                $form_attr = array('id' => 'form-security-user', 'class' => 'form-horizontal');
                $label_attr = array('class' => 'col-sm-3 control-label');
              ?>
              <?= form_open('user/update-security-user', $form_attr); ?>
                <div class="modal-body">
                  <div class="box-body">
                    <div class="row">
                      <br>
                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('New password'), 'new_password', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'new_password-modal',
                                'name' => 'new_password', 
                                'class' => 'form-control',
                                'type' => 'password',
                                'required' => 'required',
                                'maxlength' => 20,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Repeat new password'), 'repeat_new_password', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'repeat_new_password-modal',
                                'name' => 'repeat_new_password', 
                                'class' => 'form-control',
                                'type' => 'password',
                                'required' => 'required',
                                'maxlength' => 20,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>
                    </div>  
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="group-button text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ><?= _('Close'); ?></button>
                    <?php if($this->my_acl->access_control(FALSE, 'user', 'Admin', 'update_security_user_ajax')) : ?>
                      <input type="button" id="update-security-user" name="update-security-user" value="<?= _('Save'); ?>" class="btn btn-primary">
                      <div class="loading text-center" style="display: none;">
                        <br>
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <br>
                      </div>
                    <?php endif; ?>
                  </div>
                  <div id="msg-update-security-user" class="text-center" style="display: none;">
                    <!-- Muestra mensajes ajax -->
                  </div>
                  
                </div>
              <?= form_close(); ?>
            </div>

            <!-- tab delete user -->
            <div class="tab-pane" id="tab-delete-user">
              <?php
                $form_attr = array('id' => 'form-delete-user', 'class' => 'form-horizontal');
                $label_attr = array('class' => 'col-sm-3 control-label');
              ?>
              <?= form_open('user/delete-user', $form_attr); ?>
                <div class="modal-body">
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-6">
                        <br>
                        <p class="text-center text-danger">
                          <i class="fa fa-exclamation-triangle" aria-hidden="true"> </i>
                          <?= _('Once the user is deleted, the information can not be retrieved'); ?>
                        </p>
                      </div>
                    </div>  
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="group-button text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ><?= _('Close'); ?></button>
                    <?php if($this->my_acl->access_control(FALSE, 'user', 'Admin', 'delete_user_ajax')) : ?>
                      <input type="button" id="delete-user" name="delete-user" value="<?= _('Delete'); ?>" class="btn btn-danger">
                      <div class="loading text-center" style="display: none;">
                        <br>
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <br>
                      </div>
                    <?php endif; ?>
                  </div>
                  <div id="msg-delete-security-user" class="text-center" style="display: none;">
                    <!-- Muestra mensajes ajax -->
                  </div>
                </div>
              <?= form_close(); ?>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>