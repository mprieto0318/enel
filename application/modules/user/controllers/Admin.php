<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Admin extends MX_Controller {
 
	public function __construct() {
    parent::__construct();

    //if(!validate_session()) {
      //redirect('user/login');
    //}

    $this->load->model('Admin_model');
  }
 
  // Page List of Users
  public function index() {
    $result = $this->Admin_model->getRoles();

    $roles = array();
    foreach ($result->result() as $row) {
      $roles[$row->id] = $row->name;
    }

    // call helper
    add_css(datatables_css());
    add_js(datatables_js());
    add_js(array(base_url() . 'application/public/js/custom/' . $this->router->fetch_module() . '.js'));


    $data = array(
      'title' => _('List of Users'), 
      'show_title' => TRUE, 
      'page_render' => 'user/list_users',
      'body_class' => 'page-list-of-users',
      'roles' => $roles,
    );

    $this->load->view('layout/template',$data);
  }

  function ssp_list_users() {
    $inputs = $this->input->post();

    // Campos DataTable
    $draw = $inputs['draw'];

    $result = $this->Admin_model->ssp_list_users($inputs);

    $data = array();
    foreach ($result['data']->result() as $key => $value) {
      $data[$key]['id'] = $value->id;
      $data[$key]['username'] = $value->username;
      $data[$key]['email'] = $value->email;
      $data[$key]['phone'] = $value->phone;
      $data[$key]['first_name'] = $value->first_name;
      $data[$key]['last_name'] = $value->last_name;
      $data[$key]['last_login'] = $value->last_login;
      $data[$key]['active'] = $value->active;
      $data[$key]['roles_id'] = $value->roles_id;
      $data[$key]['name_role'] = $value->name_role;
      $data[$key]['created_on'] = $value->created_on;
      $explode_created = explode(' ', $value->created_on);
      $data[$key]['created_on_short'] = $explode_created[0];
    }

    $json_data = array(
      "draw"            => intval($draw),
      "recordsTotal"    => intval($result['total_data_filter']),
      "recordsFiltered" => intval($result['total_data']),
      "data"            => $data,
    );

    echo json_encode($json_data);
  }

  function update_user_ajax() {
    $inputs = $this->input->post();

    $values = array(
      'username' => $inputs['username'], 
      'email' => $inputs['email'], 
      'first_name' => $inputs['first_name'], 
      'last_name' => $inputs['last_name'], 
      'phone' => $inputs['phone'], 
      'active' => (isset($inputs['active'])) ? $inputs['active'] : 0, 
      'roles_id' => $inputs['roles_id'], 
    );
    
    $result = $this->Admin_model->update_user($inputs['id'], $values);

    //$msg = '<br><p class="text-danger">No se pudo actualizar el usuario intente mas tarde</p>';
    $msg = '<br><p class="text-danger">' . _('Edit the content to update') . ' <i class="fa fa-exclamation" aria-hidden="true"></i></p>';
    if ($result) {
      $msg = '<br><p class="text-success">' . _('Updated user') . ' <i class="fa fa-check" aria-hidden="true"></i></p>';
    }

    $update_token_ajax = array(
      'csrf_name' => $this->security->get_csrf_token_name(), 
      'csrf_hash' => $this->security->get_csrf_hash(), 
    );

    $json_data = array(
      'msg' => $msg, 
      'update_token_ajax' => $update_token_ajax, 
    );

    echo json_encode($json_data);
  }

  function update_security_user_ajax() {
    $inputs = $this->input->post();

    $msg = '';

    if ($inputs['new_password'] === $inputs['repeat_new_password']) {
      $values = array(
        'password' => sha1($inputs['new_password']), 
      );
      $result = $this->Admin_model->update_user($inputs['id'], $values);

      $msg = '<br><p class="text-danger">' . _('Could not update the user try later') . '</p>';
      if ($result) {
        $msg = '<br><p class="text-success">' . _('Updated user') . ' <i class="fa fa-check" aria-hidden="true"></i></p>';
      }
    }else {
      $msg = '<br><p class="text-danger">' . _('Passwords must be the same') . ' <i class="fa fa-exclamation" aria-hidden="true"></i></p>';
    }

    $update_token_ajax = array(
      'csrf_name' => $this->security->get_csrf_token_name(), 
      'csrf_hash' => $this->security->get_csrf_hash(), 
    );

    $json_data = array(
      'msg' => $msg, 
      'update_token_ajax' => $update_token_ajax, 
    );

    echo json_encode($json_data);
  }

  function delete_user_ajax() {
    $result = $this->Admin_model->delete_users('id', $this->input->post('id'));

    if ($result) {
      $msg = '<br><h4 class="text-success">' . _('User Deleted') . ' <i class="fa fa-check" aria-hidden="true"></i></h4>';
    }else {
      $msg = '<br><h4 class="text-danger">' . _('The user could not be deleted') . '</h4>';
    }

    $update_token_ajax = array(
      'csrf_name' => $this->security->get_csrf_token_name(), 
      'csrf_hash' => $this->security->get_csrf_hash(), 
    );

    $json_data = array(
      'msg' => $msg, 
      'update_token_ajax' => $update_token_ajax, 
    );

    echo json_encode($json_data);
  }

  function create_user_ajax() {

    $inputs = $this->input->post();

    $ok = TRUE;
    $type = '';
    $msg = '';
    $msg_ajax = '';

    // Valida contraseñas iguales
    if ($inputs['new_password'] !== $inputs['repeat_new_password']) {
      $ok = FALSE;
      $msg_ajax = '<br><p class="text-danger">' . _('Passwords must be the same') . ' <i class="fa fa-exclamation" aria-hidden="true"></i></p>';
    }

    // Valida que el username no exista
    $values = array('username' => $inputs['username']);
    $result = $this->Admin_model->get_users($values);
    if ($result->num_rows() > 0) {
      $ok = FALSE;
      $msg_ajax = '<br><p class="text-danger">' . _('Username already exist') . ' <i class="fa fa-exclamation" aria-hidden="true"></i></p>';
    }

    // Valida que el email no exista
    $values = array('email' => $inputs['email']);
    $result = $this->Admin_model->get_users($values);
    if ($result->num_rows() > 0) {
      $ok = FALSE;
      $msg_ajax = '<br><p class="text-danger">' . _('Email already exist') . ' <i class="fa fa-exclamation" aria-hidden="true"></i></p>';
    }

    if ($ok) {
      $values = array(
        'username' => $inputs['username'], 
        'email' => $inputs['email'], 
        'first_name' => $inputs['first_name'], 
        'last_name' => $inputs['last_name'], 
        'phone' => $inputs['phone'], 
        'active' => (isset($inputs['active'])) ? $inputs['active'] : 0, 
        'roles_id' => $inputs['roles_id'], 
        'password' => sha1($inputs['new_password']),
      );

      $result = $this->Admin_model->create_user($values);

      if ($result) {
        $type = 'success';
        $msg = _('Created User');
        save_messages($type, $msg);
      }else {
        $ok = FALSE;
        $msg_ajax = '<br><h4 class="text-danger">' . _('The user could not be created') . '</h4>';
      }
    }
    
    $update_token_ajax = array(
      'csrf_name' => $this->security->get_csrf_token_name(), 
      'csrf_hash' => $this->security->get_csrf_hash(), 
    );

    $json_data = array(
      'ok' => $ok,
      'msg' => $msg_ajax, 
      'update_token_ajax' => $update_token_ajax, 
    );

    echo json_encode($json_data);
  }

  public function change_pass_token($token) {

    $values = array(
      'token_mail' => $token, 
    );

    $result = $this->Admin_model->get_users(clean_data($values));

    if ($user = $result->result()[0]) {

      $new_password = $this->generate_pass();

      $data = array(
        'password' => sha1($new_password),
        'token_mail' => 'USED',
      );

      $result = $this->Admin_model->update_user($user->id, $data);

      if ($result) {
        $type = 'success';
        $msg = _('You can login with the following password: ') . ' <b>' . $new_password . '</b><br> ¡ ' . _('Remember to change the password in MY PROFILE');
        save_messages($type, $msg);
      }else {
        $type = 'danger';
        $msg = _('Could not generate a new password');
        save_messages($type, $msg);
      }
    }else {
      $type = 'danger';
      $msg = _('The token is not valid');
      save_messages($type, $msg);
    }

    redirect('/user/login');
  }

  public function generate_pass() {
    $cadena = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
    $longitudCadena = strlen($cadena);
    $pass = "";
    $longitudPass=10;
     
    for($i = 1; $i <= $longitudPass; $i++) {
      $pos = rand(0, $longitudCadena-1);
      $pass .= substr($cadena, $pos, 1);
    }

    return $pass;
  }
}