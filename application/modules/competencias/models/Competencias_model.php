<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Competencias_model extends CI_Model {

  public $tbl;

  public function __construct() {
    parent::__construct();
    $this->tbl = 'competencias';
  }

  function ssp_list($values = array()) {
    // Campos DataTable
    $start = $values['start'];
    $length = $values['length'];
    $order = $values['order'];
    $search = $values['search']['value'];

    /*$start = 0;
    $length = 10;
    $order[0]['column'] = 0;
    $order[0]['dir'] = 'ASC';
    $search = '';
    */

    // Campos custom
    $dateStart = (isset($values['dateStart'])) ? $values['dateStart'] : '';
    $dateEnd = (isset($values['dateEnd'])) ? $values['dateEnd'] : '';

    $q = $this->db->select();

    // Filtrar por buscador
    if ($search) {
      $q->like('name', $search);
      $q->or_like('description', $search);
    }

    // Filtrar por fechas
    if (!empty($dateStart) && !empty($dateEnd)) {
      $q->where('created_on >=', $dateStart . ' 00:00:00');
      $q->where('created_on <=', $dateEnd . ' 23:59:59');
    }

    // Obtener todos los registros
    if ($length != -1) {
      $q->limit($length, $start);
    }

    // Ordenar por
    $columns = array('id', 'name', 'description');
    $q->order_by($columns[$order[0]['column']], $order[0]['dir']);

    // Obtener SQL - DEBBUG
    //$sql = $q->get_compiled_select('users', FALSE);
    //echo $sql; exit;


    // Obtener datos
    $data = $q->get($this->tbl);

    // Obtener datos filtrados
    $total_data_filter = $q->count_all_results();
    //$totalDatoObtenido = $resultado->num_rows();

    // ------ Obtener total de resultados
    $q->reset_query();

    $q_count = $this->db;
    //$search = 'hola';
    // Filtrar por buscador
    if ($search) {
      $q->like('name', $search);
      $q->or_like('description', $search);
    }

    // Filtrar por fechas
    if (!empty($dateStart) && !empty($dateEnd)) {
      $q_count->where('created_on >=', $dateStart . ' 00:00:00');
      $q_count->where('created_on <=', $dateEnd . ' 23:59:59');
    }

    $q_count->from($this->tbl);
    $total_data = $q_count->count_all_results();

    return array(
      'total_data' => $total_data, 
      'data' => $data,
      'total_data_filter' => $total_data_filter,
    );
  }

  function update($id, $values) {
    $this->db->where('id', $id);
    $this->db->update($this->tbl, $values);

    return $this->db->affected_rows();
  }

  function delete($column, $value) {
    return $this->db->delete($this->tbl, array($column => $value));
  }

  function create($values) {
    return $this->db->insert($this->tbl, $values);
  }

}