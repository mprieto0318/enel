<!-- Modal Editar Usuario -->
<div id="modal-edit" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-blue">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= _('Edit'); ?></h4>
      </div>

      <div class="wrp-tabs"> 
        <ul class="nav nav-tabs">
          <li class="active">
            <a  href="#tab-update" data-toggle="tab"><i class="fa fa-user" aria-hidden="true"></i> <?= _('Update'); ?></a>
          </li>
          <li><a href="#tab-delete" data-toggle="tab"><i class="fa fa-trash-o" aria-hidden="true"></i> <?= _('Delete'); ?></a>
          </li>
        </ul>

          <div class="tab-content ">
            <!-- tab update -->
            <div class="tab-pane active" id="tab-update">
              <?php
                $form_attr = array('id' => 'form-edit', 'class' => 'form-horizontal');
                $label_attr = array('class' => 'col-sm-3 control-label');
              ?>
              <?= form_open($URL_1 . '/update', $form_attr); ?>
                <div class="modal-body">
                  <div class="box-body">
                    <div class="row">
                      <br>
                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Name'), 'name', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'name-modal',
                                'name' => 'name', 
                                'placeholder' => _('Enter the name'),
                                'class' => 'form-control',
                                'required' => 'required',
                                'maxlength' => 45,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Description'), 'description', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'description-modal',
                                'name' => 'description', 
                                'placeholder' => _('Enter the description'),
                                'class' => 'form-control',
                                'maxlength' => 250,
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Image'), 'img', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'img-modal',
                                'name' => 'file', 
                                'class' => 'form-control',
                                'type' => 'file',
                              );
                            ?>
                            <?= form_input($input); ?>
                            <div id="img-current">
                              
                            </div>
                          </div>
                        </div>
                      </div>

                      <?php
                        $input = array(
                          'id'    => 'img_current_hidden-modal',
                          'type'  => 'hidden',
                          'name'  => 'img_current_hidden-modal',
                        );
                      ?>
                      <?= form_input($input); ?>

                      <?php
                        $input = array(
                          'id'    => 'id-modal',
                          'type'  => 'hidden',
                          'name'  => 'id',
                        );
                      ?>
                      <?= form_input($input); ?>
                    </div>  
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="group-button text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ><?= _('Close'); ?></button>
                    <?php if($this->my_acl->access_control(FALSE, $MODULE, $CONTROL, 'update_ajax')) : ?>
                      <input type="button" id="btn-edit" name="btn-edit" value="<?= _('Save'); ?>" class="btn btn-primary">
                      <div class="loading text-center" style="display: none;">
                        <br>
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <br>
                      </div>
                    <?php endif; ?>
                  </div>
                  <div id="msg-update" class="text-center" style="display: none;">
                    <!-- Muestra mensajes ajax -->
                  </div>
                </div>
              <?= form_close(); ?>
            </div>

            <!-- tab delete -->
            <div class="tab-pane" id="tab-delete">
              <?php
                $form_attr = array('id' => 'form-delete', 'class' => 'form-horizontal');
                $label_attr = array('class' => 'col-sm-3 control-label');
              ?>
              <?= form_open($URL_1 . '/delete', $form_attr); ?>
                <div class="modal-body">
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-6">
                        <br>
                        <p class="text-center text-danger">
                          <i class="fa fa-exclamation-triangle" aria-hidden="true"> </i>
                          <?= _('Once deleted, the information can not be recovered'); ?>
                        </p>
                      </div>
                    </div>  
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="group-button text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ><?= _('Close'); ?></button>
                    <?php if($this->my_acl->access_control(FALSE, $MODULE, $CONTROL, 'delete_ajax')) : ?>
                      <input type="button" id="btn-delete" name="btn-delete" value="<?= _('Delete'); ?>" class="btn btn-danger">
                      <div class="loading text-center" style="display: none;">
                        <br>
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <br>
                      </div>
                    <?php endif; ?>
                  </div>
                  <div id="msg-delete" class="text-center" style="display: none;">
                    <!-- Muestra mensajes ajax -->
                  </div>
                </div>
              <?= form_close(); ?>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>