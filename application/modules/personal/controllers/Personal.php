<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Personal extends MX_Controller {
 
	public function __construct() {
    parent::__construct();

    //if(!validate_session()) {
      //redirect('user/login');
    //}

    $this->load->model('Personal_model', 'GetModel');
  }
 
  // Page List
  public function index() {
    // call helper
    add_css(datatables_css());
    add_js(datatables_js());
    add_js(array(base_url() . 'application/public/js/custom/' . $this->router->fetch_module() . '.js'));

    // Get Empresas
    $result = $this->GetModel->getEmpresas();
    $empresas = array();
    foreach ($result->result() as $row) {
      $empresas[$row->id] = $row->name;
    }

    // Get Cargos
    $result = $this->GetModel->getCargos();
    $cargos = array();
    foreach ($result->result() as $row) {
      $cargos[$row->id] = $row->name;
    }

    // Get Nivel Academico
    $result = $this->GetModel->getNivelAcademico();
    $nivel_academico = array();
    foreach ($result->result() as $row) {
      $nivel_academico[$row->id] = $row->name;
    }

    // Get Competencias
    $result = $this->GetModel->getCompetencias();
    $competencias = array();
    foreach ($result->result() as $row) {
      $competencias[$row->id] = $row->name;
    }

    $data = array(
      'MODULE' => $this->router->fetch_module(),
      'CONTROL' => $this->router->fetch_class(),
      'URL_1' => $this->uri->segment(1),
      'title' => _('List of Personal'), 
      'show_title' => TRUE, 
      'page_render' => 'personal/list',
      'body_class' => 'page-list-' . $this->uri->segment(1),
      'empresas' => $empresas,
      'cargos' => $cargos,
      'nivel_academico' => $nivel_academico,
      'competencias' => $competencias,
    );

    $this->load->view('layout/template',$data);
  }

  function ssp_list() {
    if (!$this->input->is_ajax_request()) {
      exit('No direct script access allowed');
    }

    $inputs = $this->input->post();

    // Campos DataTable
    $draw = $this->input->post('draw');

    $result = $this->GetModel->ssp_list($inputs);

    $data = array();
    foreach ($result['data']->result() as $key => $value) {
      $data[$key]['id'] = $value->id;
      $data[$key]['cc'] = $value->cc;
      $data[$key]['surname'] = $value->surname;
      $data[$key]['second_surname'] = $value->second_surname;
      $data[$key]['name'] = $value->name;
      $data[$key]['start_contract_date'] = $value->start_contract_date;
      $explode_start = explode(' ', $value->start_contract_date);
      $data[$key]['start_contract_date_short'] = $explode_start[0];
      $data[$key]['professional_registration'] = $value->professional_registration;
      $data[$key]['active'] = $value->active;
      $data[$key]['empresa_id'] = $value->empresa_id;
      $data[$key]['name_empresa'] = $value->name_empresa;
      $data[$key]['cargos_id'] = $value->cargos_id;
      $data[$key]['name_cargo'] = $value->name_cargo;
      $data[$key]['nivel_academico_id'] = $value->nivel_academico_id;
      $data[$key]['name_nivel_academico'] = $value->name_nivel_academico;
      $data[$key]['img'] = $value->img;
      $data[$key]['created_on'] = $value->created_on;
      $explode_created = explode(' ', $value->created_on);
      $data[$key]['created_on_short'] = $explode_created[0];
    }

    $json_data = array(
      "draw"            => intval($draw),
      "recordsTotal"    => intval($result['total_data_filter']),
      "recordsFiltered" => intval($result['total_data']),
      "data"            => $data,
    );

    echo json_encode($json_data);
  }

  function update_ajax() {
    if (!$this->input->is_ajax_request()) {
      exit('No direct script access allowed');
    }

    $inputs = $this->input->post();

    $values = array(
      'cc' => $inputs['cc'], 
      'surname' => $inputs['surname'],
      'second_surname' => $inputs['second_surname'],
      'name' => $inputs['name'],
      'start_contract_date' => $inputs['start_contract_date'],
      'professional_registration' => $inputs['professional_registration'],
      'active' => (isset($inputs['active'])) ? $inputs['active'] : 0,
      'empresa_id' => $inputs['empresa_id'],
      'cargos_id' => $inputs['cargos_id'],
      'nivel_academico_id' => $inputs['nivel_academico_id'],
    );

    $config =  array(
      'upload_path'     => './application/public/src/img-personal/',
      'allowed_types'   => 'jpg|png|jpeg',
      'max_size'        => "20048", // 20MB
      'overwrite'       => TRUE,
      'file_name'       => time(),
    );

    $this->load->library('upload', $config);
    $this->upload->initialize($config);

    $errorFile = '';
    $uploadLogo = false;

    if (!empty($inputs['img_current_hidden-modal']) && !empty($_FILES['file']['name'])) {

      $uploadLogo = true;

      if (!$this->upload->do_upload('file')) {
        $errorFile = $this->upload->display_errors();
      }else {
        $file_info = $this->upload->data();      
      }
    
    }else if (empty($inputs['img_current_hidden-modal']) && !empty($_FILES['file']['name'])) {
      $uploadLogo = true;

      if (!$this->upload->do_upload('file')) {
        $errorFile = $this->upload->display_errors();
      }else {
        $file_info = $this->upload->data();      
      }
    }


    if ($errorFile == '' && $uploadLogo) {
      $values['img'] = $file_info['file_name'];
    }
    
    $result = $this->GetModel->update($inputs['id'], $values);

    //$msg = '<br><p class="text-danger">No se pudo actualizar el usuario intente mas tarde</p>';
    if ($result) {
      @unlink('./application/public/src/img-personal/' . $inputs['img_current_hidden-modal']);

      $msg = '<br><p class="text-success">' . _('Updated') . ' <i class="fa fa-check" aria-hidden="true"></i></p><br><small class="text-danger">' . $errorFile . '</small>';
    }else {
      @unlink('./application/public/src/img-personal/' . $file_info['file_name']);
      $msg = '<br><p class="text-danger">' . _('Edit the content to update') . ' <i class="fa fa-exclamation" aria-hidden="true"></i></p><br><small class="text-danger">' . $errorFile . '</small>';
    }

    $update_token_ajax = array(
      'csrf_name' => $this->security->get_csrf_token_name(), 
      'csrf_hash' => $this->security->get_csrf_hash(), 
    );

    $json_data = array(
      'msg' => $msg, 
      'update_token_ajax' => $update_token_ajax, 
      'img_change' => (empty($file_info['file_name'])) ? $inputs['img_current_hidden-modal'] : $file_info['file_name'],
    );

    echo json_encode($json_data);
  }

  function delete_ajax() {
    if (!$this->input->is_ajax_request()) {
      exit('No direct script access allowed');
    }

    $result = $this->GetModel->delete('id', $this->input->post('id'));

    if ($result) {
      $msg = '<br><h4 class="text-success">' . _('Deleted') . ' <i class="fa fa-check" aria-hidden="true"></i></h4>';
    }else {
      $msg = '<br><h4 class="text-danger">' . _('The record could not be deleted') . '</h4>';
    }

    $update_token_ajax = array(
      'csrf_name' => $this->security->get_csrf_token_name(), 
      'csrf_hash' => $this->security->get_csrf_hash(), 
    );

    $json_data = array(
      'msg' => $msg, 
      'update_token_ajax' => $update_token_ajax, 
    );

    echo json_encode($json_data);
  }

  function create_ajax() {
    if (!$this->input->is_ajax_request()) {
      exit('No direct script access allowed');
    }

    $inputs = $this->input->post();

    $ok = TRUE;
    $type = '';
    $msg = '';
    $msg_ajax = '';

    $values = array(
      'cc' => $inputs['cc'], 
      'surname' => $inputs['surname'],
      'second_surname' => $inputs['second_surname'],
      'name' => $inputs['name'],
      'start_contract_date' => $inputs['start_contract_date'],
      'professional_registration' => $inputs['professional_registration'],
      'active' => (isset($inputs['active'])) ? $inputs['active'] : 0,
      'empresa_id' => $inputs['empresa_id'],
      'cargos_id' => $inputs['cargos_id'],
      'nivel_academico_id' => $inputs['nivel_academico_id'],
    );

    $config =  array(
      'upload_path'     => './application/public/src/img-personal/',
      'allowed_types'   => 'jpg|png|jpeg',
      'max_size'        => "20048", // 20MB
      'overwrite'       => TRUE,
      'file_name'       => time(),
    );

    $this->load->library('upload', $config);
    $this->upload->initialize($config);

    $errorFile = '';

    if (!$this->upload->do_upload('file')) {
      $errorFile = $this->upload->display_errors();
    }else {
      $file_info = $this->upload->data();      
    }

    if ($errorFile == '') {
      $values['img'] = $file_info['file_name'];
    }

    $result = $this->GetModel->create($values);

    if ($result) {
      $type = 'success';
      $msg = _('Created') . '<br><small class="text-danger">' . $errorFile . '</small>';
      save_messages($type, $msg);
    }else {
      $ok = FALSE;
      @unlink('./application/public/src/img-personal/' . $file_info['file_name']);
      $msg_ajax = '<br><h4 class="text-danger">' . _('Could not create the record') . '</h4><br><small class="text-danger">' . $errorFile . '</small>';
    }
    
    $update_token_ajax = array(
      'csrf_name' => $this->security->get_csrf_token_name(), 
      'csrf_hash' => $this->security->get_csrf_hash(), 
    );

    $json_data = array(
      'ok' => $ok,
      'msg' => $msg_ajax, 
      'update_token_ajax' => $update_token_ajax, 
    );

    echo json_encode($json_data);
  }

  function create_competencia_ajax() {
    if (!$this->input->is_ajax_request()) {
      exit('No direct script access allowed');
    }

    $ok = TRUE;
    $type = '';
    $msg = '';
    $msg_ajax = '';

    $inputs = $this->input->post();

    $values = array(
      'expedition_date' => $inputs['expedition_date'],
      'expiration_date' => $inputs['expiration_date'],
      'validity_time' => $inputs['validity_time'],
      'personal_id' => $inputs['personal_id'],
      'competencias_id' => $inputs['competencias_id'], 
      'active' => (isset($inputs['active'])) ? $inputs['active'] : 0, 
      'comment_report' => $inputs['comment_report'],
    );

    $config =  array(
      'upload_path'     => './application/public/src/certificados/',
      'allowed_types'   => 'pdf|doc|docx',
      'max_size'        => "20048", // 20MB
      'overwrite'       => TRUE,
      'file_name'       => $values['personal_id'] . '_' . $values['competencias_id'] . '-' . time(),
    );

    $this->load->library('upload', $config);
    $this->upload->initialize($config);

    $errorFile = '';

    if (!$this->upload->do_upload('file')) {
      $errorFile = $this->upload->display_errors();
    }else {
      $file_info = $this->upload->data();      
    }

    if ($errorFile == '') {
      $values['attached_certificate'] = $file_info['file_name'];
    }

    $result = $this->GetModel->create_competencia($values);

    if ($result) {
      $msg_ajax = '<h4 class="text-success">' . _('Created') . '<br> <small class="text-danger">' . $errorFile . '</small></h4>';
    }else {
      $ok = FALSE;
      $msg_ajax = '<h4 class="text-danger">' . _('Could not create the record') . '<br> <small class="text-danger">' . $errorFile . '</small></h4>';
    }
    
    $update_token_ajax = array(
      'csrf_name' => $this->security->get_csrf_token_name(), 
      'csrf_hash' => $this->security->get_csrf_hash(), 
    );

    $json_data = array(
      'ok' => $ok,
      'msg' => $msg_ajax, 
      'update_token_ajax' => $update_token_ajax, 
    );

    echo json_encode($json_data);
  }


  function get_edit_competencia_ajax() {
    if (!$this->input->is_ajax_request()) {
      exit('No direct script access allowed');
    }

    $inputs = $this->input->post();    

    $values = array(
      'r.id' => $inputs['id'], 
    );

    $result = $this->GetModel->listCompetencias($values);

    $result->result()[0]->expedition_date = substr($result->result()[0]->expedition_date, 0, 10);
    $result->result()[0]->expiration_date = substr($result->result()[0]->expiration_date, 0, 10);
    echo json_encode($result->result()[0]);
  }


  function edit_competencia_ajax() {
    if (!$this->input->is_ajax_request()) {
      exit('No direct script access allowed');
    }

    $inputs = $this->input->post();

    $values = array(
      'expedition_date' => $inputs['expedition_date'],
      'expiration_date' => $inputs['expiration_date'],
      'validity_time' => $inputs['validity_time'],
      //'competencias_id' => $inputs['competencias_id'], 
      'active' => (isset($inputs['active'])) ? $inputs['active'] : 0, 
      'comment_report' => $inputs['comment_report'],
    );


    $config =  array(
      'upload_path'     => './application/public/src/certificados/',
      'allowed_types'   => 'pdf|doc|docx',
      'max_size'        => "20048", // 20MB
      'overwrite'       => TRUE,
      'file_name'       => $inputs['personal_id'] . '_' . $inputs['competencias_id_hidden'] . '-' . time(),
    );

    $this->load->library('upload', $config);
    $this->upload->initialize($config);

    $errorFile = '';
    $uploadLogo = false;

    if (!empty($inputs['attached_certificate_hidden']) && !empty($_FILES['file']['name'])) {

      $uploadLogo = true;

      @unlink('./application/public/src/certificados/' . $inputs['attached_certificate_hidden']);

      if (!$this->upload->do_upload('file')) {
        $errorFile = $this->upload->display_errors();
      }else {
        $file_info = $this->upload->data();      
      }
    
    }else if (empty($inputs['attached_certificate_hidden']) && !empty($_FILES['file']['name'])) {
      $uploadLogo = true;

      if (!$this->upload->do_upload('file')) {
        $errorFile = $this->upload->display_errors();
      }else {
        $file_info = $this->upload->data();      
      }
    }


    if ($errorFile == '' && $uploadLogo) {
      $values['attached_certificate'] = $file_info['file_name'];
    }

    $result = $this->GetModel->update_competencia($inputs['id'], $values);

    //$msg = '<br><p class="text-danger">No se pudo actualizar el usuario intente mas tarde</p>';
    if ($result) {
      $msg = '<br><p class="text-success">' . _('Updated') . ' <i class="fa fa-check" aria-hidden="true"></i></p>'. '<br><small class="text-danger">' . $errorFile . '</small>';
    }else {
      $msg = '<br><p class="text-danger">' . _('Edit the content to update') . ' <i class="fa fa-exclamation" aria-hidden="true"></i></p>'. '<br><small class="text-danger">' . $errorFile . '</small>';
      @unlink('./application/public/src/certificados/' . $file_info['file_name']);
    }

    $update_token_ajax = array(
      'csrf_name' => $this->security->get_csrf_token_name(), 
      'csrf_hash' => $this->security->get_csrf_hash(), 
    );

    $json_data = array(
      'msg' => $msg, 
      'update_token_ajax' => $update_token_ajax, 
      //'file' => (empty($file_info['file_name'])) ? '' : $file_info['file_name'];
    );

    echo json_encode($json_data);
  }


  function delete_competencia_ajax() {
    if (!$this->input->is_ajax_request()) {
      exit('No direct script access allowed');
    }

    $result = $this->GetModel->delete_competencia('id', $this->input->post('id'));

    if ($result) {
      $msg = '<br><h4 class="text-success">' . _('Deleted') . ' <i class="fa fa-check" aria-hidden="true"></i></h4>';
    }else {
      $msg = '<br><h4 class="text-danger">' . _('The record could not be deleted') . '</h4>';
    }

    /*$update_token_ajax = array(
      'csrf_name' => $this->security->get_csrf_token_name(), 
      'csrf_hash' => $this->security->get_csrf_hash(), 
    );
    */

    $json_data = array(
      'msg' => $msg, 
      //'update_token_ajax' => $update_token_ajax, 
    );

    echo json_encode($json_data);
  }



  function list_competencias() {
    if (!$this->input->is_ajax_request()) {
      exit('No direct script access allowed');
    }

    $inputs = $this->input->post();    

    $values = array(
      'r.personal_id' => $inputs['id'], 
    );

    $result = $this->GetModel->listCompetencias($values);

    $data = '';
    foreach ($result->result() as $key => $value) {
      $data .= '<div class="list-group-item">
                  <div class="row">
                    <div class="col-md-12">
                      <h4 class="list-group-item-heading">- 
                        <a class="edit-competencia"  href="#" data-toggle="modal" data-target="#modal-edit-competencia" data-id_competencia="' . $value->competencias_id . '" onclick="editCompetencia(' . $value->id . ',' . $inputs['id'] . ')";>' 
                          . $value->name_competencia . 
                       '</a>
                      </h4>
                    </div>
                    <div class="col-md-6">
                      <p class="list-group-item-text"><b>' . _('Expedition Date') . ':</b> ' . $value->expedition_date . '</p>
                    </div>
                    <div class="col-md-6">
                      <p class="list-group-item-text"><b>' . _('Expiration Date') . ':</b> ' . $value->expiration_date . '</p>
                    </div>
                    <div class="col-md-6">
                      <p class="list-group-item-text"><b>' . _('Validity Time') . ':</b> ' . $value->validity_time . '</p>
                    </div>
                    <div class="col-md-6">
                      <p class="list-group-item-text"><b>' . _('Attached Certificate') . ':</b> <a href="/personal/download-file/' . $value->attached_certificate . '" target="_blank">' . _('Download') . '</a></p>
                    </div>
                  </div>
                </div>';
    }

    echo $data;
  }


  function download_file($file) {

    $this->load->helper('download');

    $data = file_get_contents(APPPATH_URI . '/public/src/certificados/' . $file);
    force_download($file, $data, TRUE);
  }

}