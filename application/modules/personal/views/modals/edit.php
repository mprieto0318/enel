<!-- Modal Editar Usuario -->
<div id="modal-edit" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-blue">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= _('Edit'); ?></h4>
      </div>

      <div class="wrp-tabs"> 
        <ul class="nav nav-tabs">
          <li class="active">
            <a  href="#tab-update" data-toggle="tab"><i class="fa fa-info" aria-hidden="true"></i> <?= _('Inf. Person'); ?></a>
          </li>
          <li><a href="#tab-competencias" data-toggle="tab"><i class="fa fa-address-card" aria-hidden="true"></i> <?= _('Inf. Competitions'); ?></a>
          </li>
          <!-- <li><a href="#tab-delete" data-toggle="tab"><i class="fa fa-trash-o" aria-hidden="true"></i> <?= _('Delete'); ?></a> -->
          </li>
        </ul>

          <div class="tab-content ">
            <!-- tab Inf. Personal -->
            <div class="tab-pane active" id="tab-update">
              <?php
                $form_attr = array('id' => 'form-edit', 'class' => 'form-horizontal');
                $label_attr = array('class' => 'col-sm-3 control-label');
              ?>
              <?= form_open($URL_1 . '/update', $form_attr); ?>
                <div class="modal-body">
                  <div class="box-body">
                    <div class="row">
                      <br>

                      <div class="col-md-12">
                        <div class="row">
                          <fieldset>
                            <legend class="text-center"><?= _('Personal Information'); ?></legend>

                            <div class="col-md-6">
                              <div class="form-group">
                                <?= form_label(_('CC'), 'cc', $label_attr); ?>
                                <div class="col-sm-9">
                                  <?php
                                    $input = array(
                                      'id' => 'cc-modal',
                                      'name' => 'cc', 
                                      'placeholder' => _('Enter the cc'),
                                      'class' => 'form-control',
                                      'required' => 'required',
                                      'maxlength' => 20,
                                    );
                                  ?>
                                  <?= form_input($input); ?>
                                </div>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <?= form_label(_('Surname'), 'surname', $label_attr); ?>
                                <div class="col-sm-9">
                                  <?php
                                    $input = array(
                                      'id' => 'surname-modal',
                                      'name' => 'surname', 
                                      'placeholder' => _('Enter the surname'),
                                      'class' => 'form-control',
                                      'required' => 'required',
                                      'maxlength' => 20,
                                    );
                                  ?>
                                  <?= form_input($input); ?>
                                </div>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <?= form_label(_('Second Surname'), 'second_surname', $label_attr); ?>
                                <div class="col-sm-9">
                                  <?php
                                    $input = array(
                                      'id' => 'second_surname-modal',
                                      'name' => 'second_surname', 
                                      'placeholder' => _('Enter the second surname'),
                                      'class' => 'form-control',
                                      'required' => 'required',
                                      'maxlength' => 20,
                                    );
                                  ?>
                                  <?= form_input($input); ?>
                                </div>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <?= form_label(_('Name'), 'name', $label_attr); ?>
                                <div class="col-sm-9">
                                  <?php
                                    $input = array(
                                      'id' => 'name-modal',
                                      'name' => 'name', 
                                      'placeholder' => _('Enter the name'),
                                      'class' => 'form-control',
                                      'required' => 'required',
                                      'maxlength' => 30,
                                    );
                                  ?>
                                  <?= form_input($input); ?>
                                </div>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <?= form_label(_('Start Contract Date'), 'start_contract_date', $label_attr); ?>
                                <div class="col-sm-9">
                                  <?php
                                    $input = array(
                                      'id' => 'start_contract_date-modal',
                                      'name' => 'start_contract_date', 
                                      'class' => 'form-control',
                                      'type' => 'date',                                );
                                  ?>
                                  <?= form_input($input); ?>
                                </div>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <?= form_label(_('Professional Registration'), 'professional_registration', $label_attr); ?>
                                <div class="col-sm-9">
                                  <?php
                                    $input = array(
                                      'id' => 'professional_registration-modal',
                                      'name' => 'professional_registration', 
                                      'placeholder' => _('Enter the professional registration'),
                                      'class' => 'form-control',
                                      'maxlength' => 45,
                                    );
                                  ?>
                                  <?= form_input($input); ?>
                                </div>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <?= form_label(_('Active'), 'active', $label_attr); ?>
                                <div class="col-sm-9">
                                  <?php
                                    $input = array(
                                      'id' => 'active-modal',
                                      'name' => 'active',
                                    );
                                  ?>
                                  <?= form_checkbox($input, 1, NULL); ?>
                                </div>
                              </div>
                            </div>
                          </fieldset>
                        </div>
                      </div>

                      <div class="col-md-12">
                        <div class="row">
                          <fieldset>
                            <legend class="text-center"><?= _('Professional Information'); ?></legend>
                            <div class="col-md-6">
                              <div class="form-group">
                                <?= form_label(_('Company'), 'empresa_id', $label_attr); ?>
                                <div class="col-sm-9">
                                  <?php
                                    $input = array(
                                      'id' => 'empresa_id-modal',
                                      'name' => 'empresa_id', 
                                      'class' => 'form-control',
                                      'options' => $empresas,
                                    );
                                  ?>
                                  <?= form_dropdown($input); ?>
                                </div>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <?= form_label(_('Appointment'), 'cargos_id', $label_attr); ?>
                                <div class="col-sm-9">
                                  <?php
                                    $input = array(
                                      'id' => 'cargos_id-modal',
                                      'name' => 'cargos_id', 
                                      'class' => 'form-control',
                                      'options' => $cargos,
                                    );
                                  ?>
                                  <?= form_dropdown($input); ?>
                                </div>
                              </div>
                            </div>


                            <div class="col-md-6">
                              <div class="form-group">
                                <?= form_label(_('Academic Level'), 'nivel_academico_id', $label_attr); ?>
                                <div class="col-sm-9">
                                  <?php
                                    $input = array(
                                      'id' => 'nivel_academico_id-modal',
                                      'name' => 'nivel_academico_id', 
                                      'class' => 'form-control',
                                      'options' => $nivel_academico,
                                    );
                                  ?>
                                  <?= form_dropdown($input); ?>
                                </div>
                              </div>
                            </div>

                            <div class="col-md-6">
                              <div class="form-group">
                                <?= form_label(_('Image'), 'img', $label_attr); ?>
                                <div class="col-sm-9">
                                  <?php
                                    $input = array(
                                      'id' => 'img-modal',
                                      'name' => 'file', 
                                      'class' => 'form-control',
                                      'type' => 'file',
                                    );
                                  ?>
                                  <?= form_input($input); ?>
                                  <div id="img-current">
                                    
                                  </div>
                                </div>
                              </div>
                            </div>

                            <?php
                              $input = array(
                                'id'    => 'img_current_hidden-modal',
                                'type'  => 'hidden',
                                'name'  => 'img_current_hidden-modal',
                              );
                            ?>
                            <?= form_input($input); ?>

                          </fieldset>
                        </div>
                      </div>

                      <?php
                        $input = array(
                          'id'    => 'id-modal',
                          'type'  => 'hidden',
                          'name'  => 'id',
                        );
                      ?>
                      <?= form_input($input); ?>
                    </div>  
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="group-button text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ><?= _('Close'); ?></button>
                    <?php if($this->my_acl->access_control(FALSE, $MODULE, $CONTROL, 'update_ajax')) : ?>
                      <input type="button" id="btn-edit" name="btn-edit" value="<?= _('Save'); ?>" class="btn btn-primary">
                      <div class="loading text-center" style="display: none;">
                        <br>
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <br>
                      </div>
                    <?php endif; ?>
                  </div>
                  <div id="msg-update" class="text-center" style="display: none;">
                    <!-- Muestra mensajes ajax -->
                  </div>
                </div>
              <?= form_close(); ?>
            </div>

            <!-- tab Inf. Professional -->
            <div class="tab-pane" id="tab-competencias">
              <?php
                $form_attr = array('id' => 'form-edit-competencias', 'class' => 'form-horizontal');
                $label_attr = array('class' => 'col-sm-3 control-label');
              ?>
              <?= form_open($URL_1 . '/update-competencias', $form_attr); ?>
                <div class="modal-body">
                  <div class="box-body">
                    <div class="row">
                      <div class="col-sm-12">
                        <h5 class="text-center">
                          <a id="new-competencia" data-personal-id="" data-personal-name="" href="#" data-toggle="modal" data-target="#modal-create-competencia" onclick="newCompetencia()";>
                            <i class="fa fa-plus" aria-hidden="true"></i> <?= _('New Competition'); ?>
                          </a>
                        </h5>
                      </div>
                      <br>
                      <br>
                      <div id="list-competencias" class="list-group">
                        <!-- Carga listado de competencias via ajax -->
                      </div>

                      <?php
                        $input = array(
                          'id'    => 'id-modal',
                          'type'  => 'hidden',
                          'name'  => 'id',
                        );
                      ?>
                      <?= form_input($input); ?>
                    </div>  
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="group-button text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ><?= _('Close'); ?></button>
                  </div>
                  <div id="msg-update-competencias" class="text-center" style="display: none;">
                    <!-- Muestra mensajes ajax -->
                  </div>
                </div>
              <?= form_close(); ?>
            </div>

            <!-- tab delete -->
            <!-- <div class="tab-pane" id="tab-delete">
              <?php
                $form_attr = array('id' => 'form-delete', 'class' => 'form-horizontal');
                $label_attr = array('class' => 'col-sm-3 control-label');
              ?>
              <?= form_open($URL_1 . '/delete', $form_attr); ?>
                <div class="modal-body">
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-6">
                        <br>
                        <p class="text-center text-danger">
                          <i class="fa fa-exclamation-triangle" aria-hidden="true"> </i>
                          <?= _('Once deleted, the information can not be recovered'); ?>
                        </p>
                      </div>
                    </div>  
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="group-button text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ><?= _('Close'); ?></button>
                    <?php if($this->my_acl->access_control(FALSE, $MODULE, $CONTROL, 'delete_ajax')) : ?>
                      <input type="button" id="btn-delete" name="btn-delete" value="<?= _('Delete'); ?>" class="btn btn-danger">
                      <div class="loading text-center" style="display: none;">
                        <br>
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <br>
                      </div>
                    <?php endif; ?>
                  </div>
                  <div id="msg-delete" class="text-center" style="display: none;">
                    
                  </div>
                </div>
              <?= form_close(); ?>
            </div> -->
          </div>
      </div>
    </div>
  </div>
</div>