<!-- Modal Editar Usuario -->
<div id="modal-create-competencia" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header bg-blue">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><?= _('New Competition'); ?></h4>
      </div>

      <div class="wrp-tabs"> 
        <ul class="nav nav-tabs">
          <li class="active">
            <a  href="#tab-create-competencia" data-toggle="tab">
              <i class="fa fa-plus-circle" aria-hidden="true"></i>
              <span id="name-person">{{name-person}}</span>
            </a>
          </li>
        </ul>

          <div class="tab-content">
            <!-- tab create-competencia -->
            <div class="tab-pane active" id="tab-create-competencia">
              <?php
                $form_attr = array('id' => 'form-create-competencia', 'class' => 'form-horizontal', 'enctype' => "multipart/form-data");
                $label_attr = array('class' => 'col-sm-3 control-label');
              ?>
              <?= form_open_multipart($URL_1 . '/create-competencia', $form_attr); ?>
                <div class="modal-body">
                  <div class="box-body">
                    <div class="row">
                      <br>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Competition'), 'competencias_id', $label_attr); ?>
                          <div class="col-sm-9 wrp-competencias">
                            <?php
                              $input = array(
                                'id' => 'competencias_id-modal',
                                'name' => 'competencias_id', 
                                'class' => 'form-control',
                                'options' => $competencias,
                              );
                            ?>
                            <?= form_dropdown($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Expedition Date'), 'expedition_date', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'expedition_date-modal',
                                'name' => 'expedition_date', 
                                'class' => 'form-control',
                                'required' => 'required',
                                'type' => 'date',                                
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Expiration Date'), 'expiration_date', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'expiration_date-modal',
                                'name' => 'expiration_date', 
                                'class' => 'form-control',
                                'type' => 'date',                                
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Validity Time'), 'validity_time', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'validity_time-modal',
                                'name' => 'validity_time', 
                                'class' => 'form-control',
                                'type' => 'number',
                                'min' => 0,
                                'max' => 50, 
                                'value' => 1,                               
                              );
                            ?>
                            <?= form_input($input); ?>
                            <small>Max: 20MB</small>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Attached Certificate'), 'attached_certificate', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'attached_certificate-modal',
                                'name' => 'file', 
                                'class' => 'form-control',
                                'type' => 'file',
                              );
                            ?>
                            <?= form_input($input); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-6">
                        <div class="form-group">
                          <?= form_label(_('Active'), 'active', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'active-modal',
                                'name' => 'active',
                              );
                            ?>
                            <?= form_checkbox($input, 1, 1); ?>
                          </div>
                        </div>
                      </div>

                      <div class="col-md-12">
                        <div class="form-group">
                          <?= form_label(_('Comment Report'), 'comment_report', $label_attr); ?>
                          <div class="col-sm-9">
                            <?php
                              $input = array(
                                'id' => 'comment_report-modal',
                                'name' => 'comment_report', 
                                'placeholder' => _('Enter the comment report'),
                                'class' => 'form-control',
                                'maxlength' => 600,
                                'rows'        => '4',
                                'cols'        => '10',
                                'value' => 'OK',
                              );
                            ?>
                            <?= form_textarea($input); ?>
                          </div>
                        </div>
                      </div>

                      <?php
                        $input = array(
                          'id'    => 'personal_id-modal',
                          'type'  => 'hidden',
                          'name'  => 'personal_id',
                        );
                      ?>
                      <?= form_input($input); ?>


                      <?php
                        $input = array(
                          'id'    => 'id-modal',
                          'type'  => 'hidden',
                          'name'  => 'id',
                        );
                      ?>
                      <?= form_input($input); ?>
                    </div>  
                  </div>
                </div>
                <div class="modal-footer">
                  <div class="group-button text-center">
                    <button type="button" class="btn btn-default" data-dismiss="modal" ><?= _('Close'); ?></button>
                    <?php if($this->my_acl->access_control(FALSE, $MODULE, $CONTROL, 'update_competencias_ajax')) : ?>
                      <input type="submit" id="btn-new-competencias" name="btn-new-competencias" value="<?= _('Save'); ?>" class="btn btn-primary">
                      <div class="loading text-center" style="display: none;">
                        <br>
                        <i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
                        <br>
                      </div>
                    <?php endif; ?>
                  </div>
                  <div id="msg-new-competencias" class="text-center" style="display: none;">
                    <!-- Muestra mensajes ajax -->
                  </div>
                </div>
              <?= form_close(); ?>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>