<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal_model extends CI_Model {

  public $tbl;

  public function __construct() {
    parent::__construct();
    $this->tbl = 'personal';
  }

  function getEmpresas($values = FALSE) {
    if ($values) {
      $this->db->where($values);
    }
    return $this->db->get('empresa');
  }

  function getCargos($values = FALSE) {
    if ($values) {
      $this->db->where($values);
    }
    return $this->db->get('cargos');
  }

  function getNivelAcademico($values = FALSE) {
    if ($values) {
      $this->db->where($values);
    }
    return $this->db->get('nivel_academico');
  }

  function getCompetencias($values = FALSE) {
    if ($values) {
      $this->db->where($values);
    }
    return $this->db->get('competencias');
  }


  function listCompetencias($values = FALSE) {

    $selects = 'r.*, c.name name_competencia';

    $q = $this->db->select($selects);
    $q->join('competencias c', 'c.id = r.competencias_id');

    if ($values) {
      $q->where($values);
    }

    $q->order_by('id', 'desc');

    return $q->get('relacion_competencia r');
  }



  function ssp_list($values = array()) {
    // Campos DataTable
    $start = $values['start'];
    $length = $values['length'];
    $order = $values['order'];
    $search = $values['search']['value'];

    /*$start = 0;
    $length = 10;
    $order[0]['column'] = 0;
    $order[0]['dir'] = 'ASC';
    $search = '';
    */

    // Campos custom
    $dateStart = (isset($values['dateStart'])) ? $values['dateStart'] : '';
    $dateEnd = (isset($values['dateEnd'])) ? $values['dateEnd'] : '';


    $selects = 'p.id, p.cc, p.surname, p.second_surname, p.name, p.start_contract_date, p.professional_registration, p.img, p.active, p.empresa_id, p.cargos_id, p.nivel_academico_id, p.created_on, e.name name_empresa, c.name name_cargo, n.name name_nivel_academico';

    $q = $this->db->select($selects);
    $q->join('empresa e', 'e.id = p.empresa_id');
    $q->join('cargos c', 'c.id = p.cargos_id');
    $q->join('nivel_academico n', 'n.id = p.nivel_academico_id');

    // Filtrar por buscador
    if ($search) {
      $q->like('p.cc', $search);
      $q->or_like('p.surname', $search);
      $q->or_like('p.second_surname', $search);
      $q->or_like('p.name', $search);
      $q->or_like('e.name', $search);
      $q->or_like('c.name', $search);
      $q->or_like('n.name', $search);
    }

    // Filtrar por fechas
    if (!empty($dateStart) && !empty($dateEnd)) {
      $q->where('p.created_on >=', $dateStart . ' 00:00:00');
      $q->where('p.created_on <=', $dateEnd . ' 23:59:59');
    }

    // Obtener todos los registros
    if ($length != -1) {
      $q->limit($length, $start);
    }

    // Ordenar por
    $columns = array('name', 'cc', 'name_empresa', 'name_cargo', 'name_nivel_academico', 'created_on');
    $q->order_by($columns[$order[0]['column']], $order[0]['dir']);

    // Obtener SQL - DEBBUG
    //$sql = $q->get_compiled_select('users', FALSE);
    //echo $sql; exit;


    // Obtener datos
    $data = $q->get($this->tbl . ' p');

    // Obtener datos filtrados
    $total_data_filter = $q->count_all_results();
    //$totalDatoObtenido = $resultado->num_rows();

    // ------ Obtener total de resultados
    $q->reset_query();

    $q_count = $this->db;
    //$search = 'hola';
    // Filtrar por buscador
    if ($search) {
      $q->like('p.cc', $search);
      $q->or_like('p.surname', $search);
      $q->or_like('p.second_surname', $search);
      $q->or_like('p.name', $search);
      $q->or_like('e.name', $search);
      $q->or_like('c.name', $search);
      $q->or_like('n.name', $search);    }

    // Filtrar por fechas
    if (!empty($dateStart) && !empty($dateEnd)) {
      $q_count->where('p.created_on >=', $dateStart . ' 00:00:00');
      $q_count->where('p.created_on <=', $dateEnd . ' 23:59:59');
    }

    $q_count->from($this->tbl . ' p');
    $q_count->join('empresa e', 'e.id = p.empresa_id');
    $q_count->join('cargos c', 'c.id = p.cargos_id');
    $q_count->join('nivel_academico n', 'n.id = p.nivel_academico_id');
    $total_data = $q_count->count_all_results();

    return array(
      'total_data' => $total_data, 
      'data' => $data,
      'total_data_filter' => $total_data_filter,
    );
  }

  function update($id, $values) {
    $this->db->where('id', $id);
    $this->db->update($this->tbl, $values);

    return $this->db->affected_rows();
  }

  function delete($column, $value) {
    return $this->db->delete($this->tbl, array($column => $value));
  }

  function create($values) {
    return $this->db->insert($this->tbl, $values);
  }

  function create_competencia($values) {
    return $this->db->insert('relacion_competencia', $values);
  }

  function update_competencia($id, $values) {
    $this->db->where('id', $id);
    $this->db->update('relacion_competencia', $values);

    return $this->db->affected_rows();
  }

  function delete_competencia($column, $value) {
    return $this->db->delete('relacion_competencia', array($column => $value));
  }

  /*function get_edit_competencia_ajax($values) {
    $selects = 'r.id, r.expedition_date, r.expiration_date, r.validity_time, r.attached_certificate, r.created_on, r.personal_id, r.competencias_id, c.name name_competencia';

    $q = $this->db->select();
    $q->join('competencias c', 'c.id = r.competencias_id');

    if ($values) {
      $q->where($values);
    }

    $q->order_by('id', 'desc');

    return $q->get('relacion_competencia r');
  }*/

}