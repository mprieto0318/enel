<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * ----- MODO DE USO -------
  $uri_public = base_url() . 'application/public/';
  1. Agregar en config.php
    //credit http://jamshidhashimi.com/2013/04/12/dynamically-add-javascript-and-css-files-in-codeigniter-header-page/

    $config['header_css'] = array();
    $config['header_js']  = array();

  2. Agregar en el controlador
  //set in view
    add_css(array($uri_public . 'js/contrib/DataTables/datatables.min.css'));
    add_js($uri_public . 'js/custom/user.js');

  3. Llamar desde la vista
    get_css()
    get_js()
  
 */

if(!function_exists('add_js')){
  function add_js($file='') {
    $str = '';
    $ci = &get_instance();
    $header_js  = $ci->config->item('header_js');

    if(empty($file)){
      return;
    }

    if(is_array($file)){
      if(!is_array($file) && count($file) <= 0){
        return;
      }
      foreach($file AS $item){
        $header_js[] = $item;
      }
      $ci->config->set_item('header_js',$header_js);
    }else{
      $str = $file;
      $header_js[] = $str;
      $ci->config->set_item('header_js',$header_js);
    }
  }
}

//Dynamically add CSS files to header page
if(!function_exists('add_css')){
  function add_css($file='')
  {
    $str = '';
    $ci = &get_instance();
    $header_css = $ci->config->item('header_css');

    if(empty($file)){
      return;
    }

    if(is_array($file)){
      if(!is_array($file) && count($file) <= 0){
        return;
      }
      foreach($file AS $item){   
        $header_css[] = $item;
      }
      $ci->config->set_item('header_css',$header_css);
    }else{
      $str = $file;
      $header_css[] = $str;
      $ci->config->set_item('header_css',$header_css);
    }
  }
}

if(!function_exists('get_css')){
  function get_css() {
    $str = '';
    $ci = &get_instance();
    $header_css = $ci->config->item('header_css');

    foreach($header_css AS $item){
      $str .= '<link rel="stylesheet" href="' . $item . '" type="text/css" />'."\n";
    }

    return $str;
  }
}

if(!function_exists('get_js')){
  function get_js() {
    $str = '';
    $ci = &get_instance();
    $header_js = $ci->config->item('header_js');

    foreach($header_js AS $item){
      $str .= '<script type="text/javascript" src="' . $item . '"></script>'."\n";
    }

    return $str;
  }
}

if(!function_exists('datatables_css')){
  function datatables_css() {
    $uri_public = base_url() . 'application/public/';

    return array( 
      $uri_public . 'js/contrib/DataTables/datatables.min.css',
      $uri_public . 'js/contrib/DataTables/Responsive-2.2.0/css/responsive.dataTables.min.css',
      $uri_public . 'js/contrib/DataTables/Buttons-1.4.2/css/buttons.dataTables.min.css',
    );
  }
}

if(!function_exists('datatables_js')){
  function datatables_js() {
    $uri_public = base_url() . 'application/public/';
    
    return array(  
      $uri_public . 'js/contrib/DataTables/datatables.min.js',
      $uri_public . 'js/contrib/DataTables/Responsive-2.2.0/js/dataTables.responsive.min.js',
      $uri_public . 'js/contrib/DataTables/Buttons-1.4.2/js/dataTables.buttons.min.js',
      $uri_public . 'js/contrib/DataTables/Buttons-1.4.2/js/buttons.html5.min.js',
      $uri_public . 'js/contrib/DataTables/Buttons-1.4.2/js/buttons.flash.min.js',
      $uri_public . 'js/contrib/DataTables/pdfmake-0.1.32/pdfmake.min.js',
      $uri_public . 'js/contrib/DataTables/pdfmake-0.1.32/vfs_fonts.js',
      $uri_public . 'js/contrib/DataTables/JSZip-2.5.0/jszip.min.js',
    );
  }
}





