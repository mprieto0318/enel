<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('validate_session')) {
  function validate_session() {
    $CI =& get_instance();
    if ($CI->session->userdata('id')) {
      return TRUE;
    }else {
      return FALSE;
    }
  }
}