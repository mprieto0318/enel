-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema enel
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `enel` ;

-- -----------------------------------------------------
-- Schema enel
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `enel` DEFAULT CHARACTER SET latin1 ;
USE `enel` ;

-- -----------------------------------------------------
-- Table `enel`.`ci_sessions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `enel`.`ci_sessions` (
  `id` VARCHAR(128) NOT NULL,
  `ip_address` VARCHAR(45) NOT NULL,
  `timestamp` INT(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` BLOB NOT NULL,
  PRIMARY KEY (`id`, `ip_address`),
  INDEX `ci_sessions_timestamp` (`timestamp` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `enel`.`login_attempts`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `enel`.`login_attempts` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` VARCHAR(45) NOT NULL,
  `login` VARCHAR(100) NOT NULL,
  `time` INT(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `enel`.`roles`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `enel`.`roles` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `enel`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `enel`.`users` (
  `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(10) NULL DEFAULT NULL,
  `password` VARCHAR(60) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `first_name` VARCHAR(50) NOT NULL,
  `last_name` VARCHAR(50) NOT NULL,
  `ip_address` VARCHAR(45) NULL DEFAULT NULL,
  `phone` VARCHAR(20) NULL DEFAULT NULL,
  `last_login` TIMESTAMP NULL DEFAULT NULL,
  `active` TINYINT(1) UNSIGNED NULL DEFAULT NULL,
  `token_mail` VARCHAR(60) NULL DEFAULT NULL,
  `token_movil` VARCHAR(45) NULL DEFAULT NULL,
  `created_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `roles_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_users_roles_idx` (`roles_id` ASC),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC),
  CONSTRAINT `fk_users_roles`
    FOREIGN KEY (`roles_id`)
    REFERENCES `enel`.`roles` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `enel`.`empresa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `enel`.`empresa` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `description` VARCHAR(250) NULL,
  `contract_number` VARCHAR(45) NOT NULL,
  `address` VARCHAR(45) NULL,
  `phone` VARCHAR(30) NULL,
  `email` VARCHAR(45) NULL,
  `img` VARCHAR(80) NULL,
  `active` INT NULL DEFAULT 1,
  `created_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `enel`.`cargos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `enel`.`cargos` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(250) NULL,
  `created_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `enel`.`nivel_academico`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `enel`.`nivel_academico` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(250) NULL,
  `created_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `enel`.`competencias`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `enel`.`competencias` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  `description` VARCHAR(250) NULL,
  `img` VARCHAR(80) NULL,
  `created_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `enel`.`personal`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `enel`.`personal` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `cc` VARCHAR(20) NOT NULL,
  `surname` VARCHAR(20) NULL,
  `second_surname` VARCHAR(20) NULL,
  `name` VARCHAR(30) NULL,
  `start_contract_date` TIMESTAMP NULL,
  `professional_registration` VARCHAR(45) NULL,
  `active` INT NULL DEFAULT 1,
  `created_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `empresa_id` INT NOT NULL,
  `cargos_id` INT NOT NULL,
  `nivel_academico_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_personal_empresa1_idx` (`empresa_id` ASC),
  INDEX `fk_personal_cargos1_idx` (`cargos_id` ASC),
  INDEX `fk_personal_nivel_academico1_idx` (`nivel_academico_id` ASC),
  CONSTRAINT `fk_personal_empresa1`
    FOREIGN KEY (`empresa_id`)
    REFERENCES `enel`.`empresa` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_personal_cargos1`
    FOREIGN KEY (`cargos_id`)
    REFERENCES `enel`.`cargos` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_personal_nivel_academico1`
    FOREIGN KEY (`nivel_academico_id`)
    REFERENCES `enel`.`nivel_academico` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `enel`.`relacion_competencia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `enel`.`relacion_competencia` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `expedition_date` DATETIME NULL,
  `expiration_date` DATETIME NULL,
  `validity_time` INT NULL,
  `attached_certificate` VARCHAR(50) NULL,
  `created_on` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `personal_id` INT NOT NULL,
  `competencias_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_relacion_competencia_personal1_idx` (`personal_id` ASC),
  INDEX `fk_relacion_competencia_competencias1_idx` (`competencias_id` ASC),
  CONSTRAINT `fk_relacion_competencia_personal1`
    FOREIGN KEY (`personal_id`)
    REFERENCES `enel`.`personal` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_relacion_competencia_competencias1`
    FOREIGN KEY (`competencias_id`)
    REFERENCES `enel`.`competencias` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `enel`.`roles`
-- -----------------------------------------------------
START TRANSACTION;
USE `enel`;
INSERT INTO `enel`.`roles` (`id`, `name`) VALUES (1, 'VISITOR');
INSERT INTO `enel`.`roles` (`id`, `name`) VALUES (2, 'OPERATOR');
INSERT INTO `enel`.`roles` (`id`, `name`) VALUES (3, 'ADMIN');
INSERT INTO `enel`.`roles` (`id`, `name`) VALUES (4, 'FULLADMIN');

COMMIT;


-- -----------------------------------------------------
-- Data for table `enel`.`users`
-- -----------------------------------------------------
START TRANSACTION;
USE `enel`;
INSERT INTO `enel`.`users` (`id`, `username`, `password`, `email`, `first_name`, `last_name`, `ip_address`, `phone`, `last_login`, `active`, `token_mail`, `token_movil`, `created_on`, `roles_id`) VALUES (1, 'fulladmin', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'prieto.miguel0318+4@gmail.com', 'full admin', 'full admin', NULL, '3138145399', '2017-11-22 20:33:54', 1, NULL, NULL, '2017-11-22 20:33:54', 4);
INSERT INTO `enel`.`users` (`id`, `username`, `password`, `email`, `first_name`, `last_name`, `ip_address`, `phone`, `last_login`, `active`, `token_mail`, `token_movil`, `created_on`, `roles_id`) VALUES (2, 'admin', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'prieto.miguel0318+3@gmail.com', 'admin', 'admin', NULL, '3138145399', '2017-12-22 20:33:54', 1, NULL, NULL, '2017-11-22 20:33:54', 3);
INSERT INTO `enel`.`users` (`id`, `username`, `password`, `email`, `first_name`, `last_name`, `ip_address`, `phone`, `last_login`, `active`, `token_mail`, `token_movil`, `created_on`, `roles_id`) VALUES (3, 'operator', '40bd001563085fc35165329ea1ff5c5ecbdbbeef', 'prieto.miguel0318+2@gmail.com', 'operator', 'operator', NULL, '3138145399', '2017-12-30 20:33:54', 1, NULL, NULL, '2017-11-22 20:33:54', 2);

COMMIT;

